#pragma once
#include "DefinedVars.h"
#include "ParticleSystem.h"
#include "AssetLib.h"
#include "CatmullRomSpline.h"

enum EnemyType {SLOW, NORMAL, FAST, BOSS};

class Enemy {
private:
	float health;
	glm::vec2 cellPos;
	glm::vec3 worldPos;
	
	vector<glm::vec2> path;
	ParticleSystem* particleSystem;
	EnemyType type;
	int value;
	float slowedTimer;
	float baseSpeed;
	float speed;
	ShadowMapParticleShader* shadowShader;

public:
	Enemy() {}
	Enemy(EnemyType type, AssetLib* assets, vector<glm::vec2> path, float statMod = 1);
	void Init();
	void Update(double deltaT, Camera& cam);
	void Draw(Camera& cam);
	void ShadowPass(glm::mat4 depthMVP);
	void SlowEnemy();
	glm::vec3 getWorldPos() { return worldPos; }
	float getParticlelife() { return particleSystem->getMaxParticleLife(); }
	void damageEnemy(float damage) { health -= damage; }
	int getValue() { return value; }
	bool isAlive() { return health > 0; }
	bool finished;
	float deathTimer;
	
	glm::vec3 lightColour;

	~Enemy(){
		delete particleSystem;
	}
};