#pragma once
#include<gl\glew.h>

class ShadowMapFBO
{
public:
	ShadowMapFBO();

	~ShadowMapFBO();

	void Init(unsigned int WindowWidth, unsigned int WindowHeight);

	void BindForWriting();

	void BindForReading(GLenum TextureUnit);

private:
	GLuint m_fbo;
	GLuint m_shadowMap;
};