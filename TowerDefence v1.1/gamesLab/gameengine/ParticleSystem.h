#pragma once
#include "headers.h"
#include "ParticleShader.h"
#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include "glm\gtc\matrix_inverse.hpp"
#include "ShadowMapParticleShader.h"

enum EmmisionDirection {FORWARD, BACKWARDS, INWARDS, OUTWARDS, ABOVE, BELOW, RANDOM};
enum ParticleScaling {STATIC, GROW, SHRINK};

struct Particle {
	glm::vec3 pos, speed;
	unsigned char r, g, b, a; // Color
	float size, angle, weight;
	float life; // Remaining life of the particle. if <0 : dead and unused.
	float cameradistance; // *Squared* distance to the camera. if dead : -1.0f

	bool operator<(const Particle& that) const {
		// Sort in reverse order : far particles drawn first.
		return this->cameradistance > that.cameradistance;
	}
};

struct ParticleProperties {
	bool gravity;
	float spread;
	float maxLife;
	bool directed;
	float maxSize;
	float minSize;
	ParticleScaling scaleType;
	float alphaFactor;

	ParticleProperties() {
		gravity = false;
		spread = 2;
		maxLife = 3;
		directed = true;
		maxSize = 0.5;
		minSize = 0.1;
		scaleType = STATIC;
		alphaFactor = 10;
	}

	ParticleProperties(bool g, float s, float ml, bool d, float maxS, float minS, ParticleScaling st, float af) {
		gravity = g;
		spread = s;
		maxLife = ml;
		directed = d;
		maxSize = maxS;
		minSize = minS;
		scaleType = st;
		alphaFactor = af;
	}

};

struct EmmiterProperties {
	glm::vec3 centerOffset;
	glm::vec3 rotationAxis;
	float rotationSpeed;
	EmmisionDirection dir;
	float emmitionForce;
	glm::vec3 axisInfulence;
	float pulseRate;
	int maxParticles;
	bool rotateAboutMoveDir;
	glm::vec3 spawnSpread;

	EmmiterProperties() {
		centerOffset = glm::vec3(3, 0, 0);
		rotationAxis = glm::vec3(0, 1, 0);
		rotationSpeed = 180;
		dir = OUTWARDS;
		emmitionForce = 1;
		axisInfulence = glm::vec3(1, 1, 1);
		pulseRate = 0;
		maxParticles = 1000;
		rotateAboutMoveDir = false;
		spawnSpread = glm::vec3(0, 0, 0);
	}

	EmmiterProperties(glm::vec3 co, glm::vec3 ra, float rs, bool ramd, EmmisionDirection d, float ef, glm::vec3 ai, float pr, glm::vec3 ss, int mp) {
		centerOffset = co;
		rotationAxis = ra;
		rotationSpeed = rs;
		dir = d;
		emmitionForce = ef;
		axisInfulence = ai;
		pulseRate = pr;
		maxParticles = mp;
		rotateAboutMoveDir = ramd;
		spawnSpread = ss;
	}

};



class ParticleSystem {
static const int maxParticles = 1000;
private:
	glm::vec3 emitterPos;
	glm::vec3 systemCentre;
	//Particle particles[maxParticles];
	//Particle* particles;
	vector<Particle> particles;
	int lastParticleUsed = 0;
	int particleCount;
	ParticleShader* shader;
	pair<Texture*, glm::vec3> texture;
	GLfloat* g_positionData;
	GLubyte* g_colourData;
	GLfloat* g_lifeData;
	GLuint VAO;
	GLuint buffers[2];

	double moveTime;
	double pulseTimer;
	double lastSpawnTimer;
	ParticleProperties pProps;
	EmmiterProperties eProps;

public:

	ParticleSystem();
	ParticleSystem(ParticleProperties pp, EmmiterProperties ep, glm::vec3 sc, ParticleShader* ps, pair<Texture*, glm::vec3> tex);
	int FindUnusedParticle();
	void SortParticles();
	void init();
	void update(double deltaT, Camera& camera);
	void draw(Camera& camera);
	void shadowMapPass(ShadowMapParticleShader* smps);

	glm::vec3 getSystemCenter() { return systemCentre; }
	void setSystemCenter(glm::vec3 systemCenter) { 
		if (systemCenter != systemCentre && eProps.rotateAboutMoveDir) {
			eProps.rotationAxis = systemCentre - systemCenter;
		}
		this->systemCentre = systemCenter; 
	
	}
	bool hasParticles() { return particleCount > 0; }
	float getMaxParticleLife() { return pProps.maxLife; }
	bool alive;

	static ParticleProperties GravDirMedStatic;
	static ParticleProperties DirSmallStatic;
	static ParticleProperties GravSmallGrow;
	static ParticleProperties DirMedShrink;
	static ParticleProperties TurretFireBall;
	static ParticleProperties FreezeElectricBolt;
	static ParticleProperties MotarCanonBall;
	static ParticleProperties PathParticles;
	static ParticleProperties TowerSmoke;
	static ParticleProperties PortalParticles;
	static ParticleProperties Butterflys;
	static ParticleProperties RubbleParticles;
	static ParticleProperties SelectParticles;
	static ParticleProperties Confetti;

	static EmmiterProperties BelowForcedXZPulse;
	static EmmiterProperties OrbitZInwardXZ;
	static EmmiterProperties OrbitYBackward;
	static EmmiterProperties OrbitXOut;
	static EmmiterProperties TurretFireBallEmmit;
	static EmmiterProperties FreezeElectricBoltEmmit;
	static EmmiterProperties MotarCanonBallEmmit;
	static EmmiterProperties PathEmmiter;
	static EmmiterProperties TowerSmokeEmmit;
	static EmmiterProperties PortalEmmit;
	static EmmiterProperties ButterflyEmmit;
	static EmmiterProperties RubbleEmmit;
	static EmmiterProperties SelectEmmit;
	static EmmiterProperties ExplosionEmmit;

	~ParticleSystem() {
		delete[] g_positionData;
		delete[] g_colourData;
		delete[] g_lifeData;
	}
};