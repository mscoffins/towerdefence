#pragma once
#include "Shader\Shader.h"

class ParticleShader : public Shader {
private:
	GLuint ViewProjMatrixID;
	GLuint nearPlaneHeightID;
	GLuint maxLifeID;
	GLuint m_ssDimentionsLocation;
	GLuint m_alphaFactorLocation;
public:

	ParticleShader() {}
	ParticleShader(const string& filename):Shader(filename){
		ViewProjMatrixID = glGetUniformLocation(main_program, "VP");
		nearPlaneHeightID = glGetUniformLocation(main_program, "nearPlaneHeight");
		maxLifeID = glGetUniformLocation(main_program, "maxLife");
		m_ssDimentionsLocation = glGetUniformLocation(main_program, "ssDims");
		m_alphaFactorLocation = glGetUniformLocation(main_program, "alphaFactor");
	}

	void Update(Camera& c, float maxLife, glm::vec3 ssDims, float alphaFactor) {

		glm::mat4 VP = c.getProjectionMatrix() * c.getView();
		glUniformMatrix4fv(ViewProjMatrixID, 1, GL_FALSE, &VP[0][0]);
		float heightOfNearPlane = 800.0f / (2 * tan(0.5*70*3.1415 / 180.0));
		glUniform1f(nearPlaneHeightID, heightOfNearPlane);
		glUniform1f(maxLifeID, maxLife);
		glUniform3f(m_ssDimentionsLocation, ssDims.x, ssDims.y, ssDims.z);
		glUniform1f(m_alphaFactorLocation, alphaFactor);
	}
};