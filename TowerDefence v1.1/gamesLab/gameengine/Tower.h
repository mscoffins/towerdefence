#pragma once
#include "ObjectLoading\Assimp\AssimpModel.h"
#include "Enemy.h"

enum TowerType {WALL, TURRET, FREEZE, MORTAR};

class Tower {
private:
	AssimpModel* model;
	glm::vec2 cellPos;
	glm::vec3 worldPos;
	glm::vec3 emmitionPos;
	Enemy* target;

	float rateOfFire;
	float fireDelay;
	float range;
	float projSpeed;

	

	TowerType type;
	ParticleProperties projProps;
	EmmiterProperties projEmmitProps;
	vector<ParticleSystem*> reserveProjectiles;
	//vector<ParticleSystem*> activeProjectiles;
	pair<Texture*, glm::vec3> projectileTex;
	ParticleShader* projectileShader;
	ShadowMapParticleShader* projectileShadowShader;
	LightingShader* modelShader;
	ShadowMapShader* modelShadowShader;
	Mix_Chunk* fireSound;
public:
	Tower() {}
	Tower(TowerType type, AssetLib* assets, glm::vec2 pos);
	void Init();
	void Update(double deltaT, Camera& cam);
	void Draw(Camera& cam, glm::mat4 depthMVP);
	void DrawProjectiles(Camera& cam);
	void ShadowMapPass(glm::mat4 depthMVP);
	void setTarget(Enemy* enemy) { target = enemy; }
	TowerType getType() { return type; }
	glm::vec3 getWorldPos() { return worldPos; }
	float getRange() { return range; }
	Enemy* getTarget() { return target; }
	vector<ParticleSystem*> getProjectiles() { return reserveProjectiles; }
	glm::vec2 getCellPos() { return cellPos; }

	void upgradeTower();

	int upgradeCost;
	int towerLevel;
	float damage;


	static int costs[4];

	~Tower() {
		for each (ParticleSystem* ps in reserveProjectiles)
		{
			ParticleSystem* psys = ps;
			ps = nullptr;
			delete psys;
		}
		reserveProjectiles.clear();
	}
};