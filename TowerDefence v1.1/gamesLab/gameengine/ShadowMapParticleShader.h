#pragma once
#include "Shader\Shader.h"

class ShadowMapParticleShader : public Shader {

public:

	ShadowMapParticleShader() {}

	ShadowMapParticleShader(const std::string& filename);

	void SetWVP(const glm::mat4 WVP);
	void setMaxLife(float maxLife);
	void setTextureLocation(unsigned int loc);
	void setSpriteSheetDimmentions(glm::vec3 dims);
private:

	GLuint m_MVPLocation;
	GLuint m_maxLifeLocation;
	GLuint m_textureLocation;
	GLuint m_ssDimentionsLocation;
};