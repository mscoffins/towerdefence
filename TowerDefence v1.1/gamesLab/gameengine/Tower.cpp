#include "Tower.h"

int Tower::costs[4] = { 100,500,300,1000 };

Tower::Tower(TowerType type, AssetLib * assets, glm::vec2 pos)
{
	this->type = type;
	cellPos = pos;
	worldPos = glm::vec3(cellPos.x * 10 - 45, -20, cellPos.y * 10 - 45);
	target = nullptr;
	fireDelay = 0;
	projSpeed = 15;
	upgradeCost = costs[type] / 4;
	towerLevel = 1;
	model = assets->towers[type];
	switch (type) {
	case WALL:
		range = 0;
		rateOfFire = 0;
		damage = 0;
		break;
	case TURRET:
		range = 30;
		rateOfFire = 1;
		damage = 50;
		projectileTex = assets->particleTextures[4];
		projProps = ParticleSystem::TurretFireBall;
		projEmmitProps = ParticleSystem::TurretFireBallEmmit;
		emmitionPos = glm::vec3(3, 17, 3);
		fireSound = assets->turretFire;
		break;
	case MORTAR:
		range = 50;
		rateOfFire = 3;
		damage = 100;
		projectileTex = assets->particleTextures[7];
		projProps = ParticleSystem::MotarCanonBall;
		projEmmitProps = ParticleSystem::MotarCanonBallEmmit;
		emmitionPos = glm::vec3(0, 21, 0);
		fireSound = assets->mortarFire;
		break;
	case FREEZE:
		range = 20;
		rateOfFire = 0.5;
		damage = 10;
		projectileTex = assets->particleTextures[6];
		projProps = ParticleSystem::FreezeElectricBolt;
		projEmmitProps = ParticleSystem::FreezeElectricBoltEmmit;
		emmitionPos = glm::vec3(3, 8, 3);
		fireSound = assets->freezeFire;
		break;
	}
	
	projectileShader = assets->particleShader;
	projectileShadowShader = assets->particleShadowShader;
	modelShader = assets->lightingShader;
	modelShadowShader = assets->shadowShader;
}

void Tower::Init()
{
	int ammo = ((range / projSpeed) / rateOfFire);
	
	for (int i = 0; i < ammo; i++) {
		reserveProjectiles.push_back(new ParticleSystem(projProps, projEmmitProps, worldPos + emmitionPos, projectileShader, projectileTex));
		reserveProjectiles[i]->init();
		reserveProjectiles[i]->alive = false;
	}

}

void Tower::Update(double deltaT, Camera& cam)
{

	if (worldPos.y != 5) {
		worldPos.y += 5 * deltaT;
		if (pow(5 - worldPos.y, 2) < 5 * deltaT)
			worldPos.y = 5;
	}
	emmitionPos = glm::vec3(glm::rotate((float)(360.0f * deltaT), glm::vec3(0, 1, 0)) * glm::vec4(emmitionPos, 0));
	if (type != WALL) {
		fireDelay -= deltaT;
		if (target != nullptr && target->isAlive() && !target->finished) {
			if (fireDelay < 0) {
				if (glm::length(worldPos - target->getWorldPos()) < range && reserveProjectiles.size() > 0) {	
					for (int i = 0; i < reserveProjectiles.size(); i++) {
						if (!reserveProjectiles[i]->alive) {
							reserveProjectiles[i]->alive = true;
							int chan = Mix_PlayChannel(-1, fireSound, 0);
							if (chan != -1) {
								float diff = MIX_MAX_VOLUME - ((glm::length(reserveProjectiles[i]->getSystemCenter() - cam.getPos())));
								if (diff < 0)
									Mix_Volume(chan, 0);
								else
									Mix_Volume(chan, diff/4);
							}
							fireDelay = rateOfFire;
							break;

						}

					}
				}
			}
		}
		int count = 0;
		for each (ParticleSystem* ps in reserveProjectiles)
		{

			if (target != nullptr && target->isAlive() && !target->finished && ps->alive) {
				glm::vec3 moveVec = target->getWorldPos() - ps->getSystemCenter();
				if (glm::length(moveVec) > 1) {
					moveVec = glm::normalize(moveVec);
					ps->setSystemCenter(ps->getSystemCenter() + moveVec * (float)(projSpeed * deltaT));

				}
				else {
					target->damageEnemy(damage);
					if (type == FREEZE)
						target->SlowEnemy();
					ps->alive = false;
					ps->setSystemCenter(worldPos + emmitionPos);
				}
				if (glm::length(worldPos - target->getWorldPos()) > range) {
					ps->alive = false;
					ps->setSystemCenter(worldPos + emmitionPos);
				}
			}
			else {
				glm::vec3 offset = glm::vec3(glm::rotate((float)((count*(360.0f / reserveProjectiles.size()))), glm::vec3(0, 1, 0)) * glm::vec4(emmitionPos, 0));
				ps->setSystemCenter(worldPos + offset);
			}
			ps->update(deltaT, cam);
			count++;
		}
	}
}

void Tower::Draw(Camera& cam, glm::mat4 depthMVP)
{
	modelShader->Bind();
	modelShader->SetWVP(cam.getProjectionMatrix() * cam.getView() * glm::translate(worldPos));
	modelShader->SetLightWVP(depthMVP * glm::translate(worldPos));
	modelShader->SetWorldMatrix(glm::translate(worldPos));

	model->BindModelsVAO();
	model->RenderModel();
	
}

void Tower::DrawProjectiles(Camera & cam)
{
	for each(ParticleSystem* p in reserveProjectiles)
		p->draw(cam);
}

void Tower::ShadowMapPass(glm::mat4 depthMVP)
{
	modelShadowShader->Bind();
	modelShadowShader->SetWVP(depthMVP * glm::translate(worldPos));
	model->BindModelsVAO();
	model->RenderModel();

	projectileShadowShader->Bind();
	projectileShadowShader->SetWVP(depthMVP);
	for each (ParticleSystem* p in reserveProjectiles)
	{
		p->shadowMapPass(projectileShadowShader);
	}

}

void Tower::upgradeTower()
{
	damage *= 1.3;
	range *= 1.2;
	upgradeCost *= 2;
	towerLevel++;
}
