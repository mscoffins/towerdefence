#pragma once
#include "Shader\Shader.h"
#include <vector>
#define MAX_POINT_LIGHTS 120

struct BaseLight {
	glm::vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
};

struct DirectionalLight:BaseLight {
	glm::vec3 Direction;
};

struct PointLight :BaseLight {
	glm::vec3 Position;
	struct
	{
		float Constant;
		float Linear;
		float Exp;
	} Attenuation;

};

class LightingShader : public Shader {
public:

	LightingShader() {}
	LightingShader(const std::string& filename);

	void SetWVP(glm::mat4 WVP);
	void SetWorldMatrix(glm::mat4 WVP);
	void SetTextureUnit(unsigned int TextureUnit);
	void SetDirectionalLight(const DirectionalLight& Light);
	void SetEyeWorldPos(const glm::vec3& EyeWorldPos);
	void SetMatSpecularIntensity(float Intensity);
	void SetMatSpecularPower(float Power);
	void SetLightWVP(glm::mat4 lWVP);
	void SetShadowMap(unsigned int ShadowMapUint);

	void SetPointLights(vector<PointLight> pLights);

private:
	GLuint m_WVPLocation;
	GLuint m_WorldMatrixLocation;
	GLuint m_samplerLocation;
	GLuint m_eyeWorldPosLocation;
	GLuint m_matSpecularIntensityLocation;
	GLuint m_matSpecularPowerLocation;
	GLuint m_lightWVPLocation;
	GLuint m_shadowMapLocation;

	GLuint m_numPointLightsLocation;

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint Direction;
		GLuint DiffuseIntensity;
	} m_dirLightLocation;

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Position;
		struct
		{
			GLuint Constant;
			GLuint Linear;
			GLuint Exp;
		} Atten;
	} m_pointLightsLocation[MAX_POINT_LIGHTS];

};