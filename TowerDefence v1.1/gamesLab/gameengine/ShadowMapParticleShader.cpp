#include "ShadowMapParticleShader.h"

ShadowMapParticleShader::ShadowMapParticleShader(const std::string & filename):Shader(filename)
{
	m_MVPLocation = glGetUniformLocation(main_program, "gLightWVP");
	m_maxLifeLocation = glGetUniformLocation(main_program, "maxLife");
	m_textureLocation = glGetUniformLocation(main_program, "myTextureSampler");
	m_ssDimentionsLocation = glGetUniformLocation(main_program, "ssDims");
}

void ShadowMapParticleShader::SetWVP(const glm::mat4 WVP)
{
	glUniformMatrix4fv(m_MVPLocation, 1, GL_FALSE, &WVP[0][0]);
}

void ShadowMapParticleShader::setMaxLife(float maxLife)
{
	glUniform1f(m_maxLifeLocation, maxLife);
}

void ShadowMapParticleShader::setTextureLocation(unsigned int loc)
{
	glUniform1i(m_textureLocation, loc);
}

void ShadowMapParticleShader::setSpriteSheetDimmentions(glm::vec3 dims)
{
	glUniform3f(m_ssDimentionsLocation, dims.x, dims.y, dims.z);
}