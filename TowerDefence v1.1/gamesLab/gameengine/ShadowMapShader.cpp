#include "ShadowMapShader.h"

ShadowMapShader::ShadowMapShader(const std::string & filename):Shader(filename)
{
	m_MVPLocation = glGetUniformLocation(main_program, "gLightWVP");
}

void ShadowMapShader::SetWVP(const glm::mat4 WVP)
{
	glUniformMatrix4fv(m_MVPLocation, 1, GL_FALSE, &WVP[0][0]);
}

