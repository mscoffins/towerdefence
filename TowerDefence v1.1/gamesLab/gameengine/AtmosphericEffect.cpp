#include "AtmosphericEffect.h"

AtmosphericEffect::AtmosphericEffect(AssetLib * assets, glm::vec3 worldPos, EffectType type)
{
	switch (type) {
	case TOWER_BUILD_SMOKE:
		effect = new ParticleSystem(ParticleSystem::TowerSmoke, ParticleSystem::TowerSmokeEmmit, worldPos, assets->particleShader, assets->particleTextures[8]);
		duration = 5;
		break;
	case PORTAL_EMMISION:
		effect = new ParticleSystem(ParticleSystem::PortalParticles, ParticleSystem::PortalEmmit, worldPos, assets->particleShader, assets->particleTextures[10]);
		duration = FLT_MAX;
		break;
	case BUTTERFLYS:
		effect = new ParticleSystem(ParticleSystem::Butterflys, ParticleSystem::ButterflyEmmit, worldPos, assets->particleShader, assets->particleTextures[11]);
		duration = FLT_MAX;
		break;
	case RUBBLE:
		effect = new ParticleSystem(ParticleSystem::RubbleParticles, ParticleSystem::RubbleEmmit, worldPos, assets->particleShader, assets->particleTextures[16]);
		duration = 2;
		break;
	case SELECTED_TOWER:
		effect = new ParticleSystem(ParticleSystem::SelectParticles, ParticleSystem::SelectEmmit, worldPos, assets->particleShader, assets->particleTextures[17]);
		duration = FLT_MAX;
		break;
	case CONFETTI_EXPLOSION:
		effect = new ParticleSystem(ParticleSystem::Confetti, ParticleSystem::ExplosionEmmit, worldPos, assets->particleShader, assets->particleTextures[18]);
		duration = 1;
		break;
	}
	
	finished = false;
	shadowShader = assets->particleShadowShader;
	deathTimer = effect->getMaxParticleLife();
}

void AtmosphericEffect::init()
{
	effect->init();
}

void AtmosphericEffect::draw(Camera & cam)
{
	if(!finished)
		effect->draw(cam);
}

void AtmosphericEffect::update(Camera & cam, double deltaT)
{
	if (!finished) {
		duration -= deltaT;
		if (duration <= 0) {
			effect->setSystemCenter(glm::vec3(-1000, -1000, -1000));
			deathTimer -= deltaT;
			if (deathTimer <= 0)
				finished = true;
		}
		effect->update(deltaT, cam);
	}
}

void AtmosphericEffect::shadowMapPass(glm::mat4 depthMVP)
{
	if (!finished) {
		shadowShader->Bind();
		shadowShader->SetWVP(depthMVP);
		effect->shadowMapPass(shadowShader);
	}

}
