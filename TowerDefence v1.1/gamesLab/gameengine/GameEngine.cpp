#include "headers.h"
#include "../ObjectLoading/Assimp/AssimpModel.h"
#include "LightingShader.h"
#include "ShadowMapFBO.h"
#include "ShadowMapShader.h"
#include "ShadowMapParticleShader.h"
#include "AssetLib.h"
#include "AStar\AStar.h"
#include "Enemy.h"
#include "Tower.h"
#include "PathDrawer.h"
#include "Wave.h"
#include "DefinedVars.h"
#include "AtmosphericEffect.h"
#include "GUI\GUI.h"
#include "gameobjects\Primitives\Ray.h"
#include "GameWorld.h"
#include "Menu.h"
#include "../include/SDL2/SDL_mixer.h"

using namespace std;

//Program Variables
int mouseX = 0, mouseY = 0;
glm::vec4 normalisedCoords;

glm::mat4 ProjectionMatrix;

glm::mat4 depthMVP;

DisplayWindow* display;

Camera camera;
//Simulation variables
ShadowMapFBO* smFBO;
DirectionalLight dirLight;
AssetLib assets;

Menu* menu;
GameWorld gameWorld;

//DeltaT using c++
double previousTime;
int frame_accum = 0;
double startTime = 0.0;
int fps = 0;
int frameCount = 0;
//Protyping methods
void keyboardProcess(double deltaT);
void eventProcess(double deltaT);
void shadowMapPass();
//KeyboardInput keyboard(true);
SDL_Event event;
const Uint8* keyState = SDL_GetKeyboardState(NULL);
bool isFinished = false, mouseDown = false;

float runSpeed = 1;


Mix_Music *gMusic = NULL;

void Reshape(int width, int height) {

	glViewport(0, 0, width, height);						// Reset The Current Viewport

															//Set the projection matrix
	ProjectionMatrix = glm::perspective(70.0f, (GLfloat)width / (GLfloat)height, 0.1f, 1000.0f);
}


void Init() {

	display->loadLoading();

	assets.LoadAssets();
	menu = new Menu(assets.guiShader);
	menu->Init();
	camera = Camera(glm::vec3(0, 50, -15), &ProjectionMatrix);
	smFBO = new ShadowMapFBO();
	smFBO->Init(2048, 2048);
	gameWorld = GameWorld(&assets);
	gameWorld.Init();

	dirLight.Color = glm::vec3(0.3, 0.6, 1);
	dirLight.AmbientIntensity = 0.2;
	dirLight.DiffuseIntensity = 0.3;
	dirLight.Direction = glm::vec3(0.2, -0.4, 0.2);

	Mix_PlayMusic(assets.bgMusic, -1);
	Mix_VolumeMusic(MIX_MAX_VOLUME/8);
}

void shadowMapPass() {
	smFBO->BindForWriting();	
	glViewport(0, 0, 2048, 2048);
	glClear(GL_DEPTH_BUFFER_BIT);
	glm::mat4 depthProjectionMatrix = glm::ortho<float>(-100, 100, -100, 100, -100, 100);
	glm::mat4 depthViewMatrix = glm::lookAt( -dirLight.Direction, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 depthModelMatrix = glm::mat4(1.0);
	depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;

	gameWorld.ShadowMapPass(depthMVP);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, WIDTH, HEIGHT);
}

void Draw() {
	if (menu->playGame) {
		shadowMapPass();

		display->ClearColour(0.9f, 0.9f, 0.9f, 1.0f);
		gameWorld.Draw(camera, normalisedCoords, depthMVP, mouseX, mouseY, smFBO, dirLight);
	}
	else {
		display->ClearColour(0.9f, 0.9f, 0.9f, 1.0f);
		menu->Draw(normalisedCoords);
	}

}
void Update(double deltaT) {
	// Obtain mouse coordinates
	SDL_GetMouseState(&mouseX, &mouseY);

	//Get the screen coordinates of the mouse
	normalisedCoords = glm::vec4(
		(2.0f*mouseX) / WIDTH - 1.0f,	// x
		1.0f - (2.0f*mouseY) / HEIGHT,  // y
		0.5f,							// z
		1.0f							// w
	);

	if(menu->playGame)
		gameWorld.Update(deltaT, camera);

}

int main(int argc, char** argv)
{
	// Setup OpenGL
	SDL_PumpEvents();
	//SDL_ShowCursor(0); // Hide mouse
	//SDL_SetRelativeMouseMode(SDL_TRUE); // Keep track of mouse and keep inside of window
	display = new DisplayWindow(WIDTH, HEIGHT, "Game Engine"); // Create window
	Reshape(WIDTH,HEIGHT);
	Init();
	
	while (!isFinished)
	{
		//programTime = GetTickCount();
		//**************************DELTAT USING C++*********************************
		// Calculate delta time
		double currentTime = GetTickCount();
		double deltaT = (currentTime - previousTime) / 1000.0f;
		if (deltaT > 0.032)
			deltaT = 0.032;

		double baseDT = deltaT;

		if(gameWorld.waveDelay <= 0)
			deltaT *= runSpeed;
		previousTime = currentTime;

		frame_accum++;
		if (currentTime - startTime >= 1000.0f)
		{
			stringstream title;
			title << "Game Engine - FPS [" << frame_accum << "]";
			display->setWindowTitle(title.str().c_str());
			startTime = currentTime;
			frame_accum = 0;
		}
		//Reshape(WIDTH, HEIGHT);

		//float tempTime = GetTickCount();
		Update(deltaT);
		//updateTime = GetTickCount() - tempTime;

		//tempTime = GetTickCount();
		Draw();
		//drawTime = GetTickCount() - tempTime;
		// Update display
		//display->Update();	

		glFlush();

		//tempTime = GetTickCount();
		//Mouse interaction
		SDL_GL_SwapWindow(display->main_Window); // Swap buffer window
		//SDL_Event e;
		while (SDL_PollEvent(&event)) //Check for mouse events
		{	
			eventProcess(baseDT);
		}
		keyboardProcess(baseDT);

	}
	return 0;
}

void keyboardProcess(double deltaT){
		event.key.keysym.sym;

		glm::vec3 camMoveDir;
		int camRotDir = 0;

		

		/* Keyboard event */

		//Camera Controls

		if (keyState[SDL_SCANCODE_W]) {
			camMoveDir.y = 1;
		}


		if (keyState[SDL_SCANCODE_S])
			camMoveDir.y = -1;

		if (keyState[SDL_SCANCODE_A])
			camMoveDir.x = -1;

		if (keyState[SDL_SCANCODE_D])
			camMoveDir.x = 1;

		if (keyState[SDL_SCANCODE_UP]) {
			if(runSpeed < 4)
				runSpeed += 0.2;
			if (runSpeed > 4)
				runSpeed = 4;
		}

		if (keyState[SDL_SCANCODE_DOWN]) {
			if (runSpeed > 1)
				runSpeed -= 0.2;
			if (runSpeed < 1)
				runSpeed = 1;
		}

		if (keyState[SDL_SCANCODE_Q])
			camRotDir = -1;

		if (keyState[SDL_SCANCODE_E])
			camRotDir = 1;

		camera.moveCam(camMoveDir, deltaT);
		camera.rotateCam(camRotDir, deltaT);


		if (keyState[SDL_SCANCODE_ESCAPE]) {
			if (menu->playGame)
				menu->playGame = false;
		}

		if (keyState[SDL_SCANCODE_C])
			gameWorld.waveDelay = 0;

		if (gameWorld.gameOver)
			if (keyState[SDL_SCANCODE_RETURN])
				gameWorld.resetWorld();

}

void eventProcess(double deltaT)
{

	switch (event.type)
	{

	case SDL_MOUSEWHEEL:
	{
		int camZoomDir = 0;

		if (event.wheel.y < 0) {
			camZoomDir = -1;
		}
		else {
			camZoomDir = 1;
		}
		camera.zoomCam(camZoomDir, 1);
	}
	break;

	case SDL_MOUSEBUTTONDOWN:
	{
		if (menu->playGame) {
			if (event.button.button == SDL_BUTTON_LEFT) {

				if (gameWorld.waveDelay > 0) {
					if (gameWorld.gui->checkGUIcollision(normalisedCoords)) {
						gameWorld.gui->checkButtonCollision(normalisedCoords);
					}
					else {
						//process placing tower in current cell if its viable
						if (gameWorld.gui->selectedTower != -1) {
							gameWorld.addTower();
						}
						else {
							gameWorld.selectTower(mouseX, mouseY, camera);
						}

					}
				}
			}
			else if (event.button.button == SDL_BUTTON_RIGHT) {
				gameWorld.gui->resetGUI();
			}
		}
		else {
			menu->buttonCollision(normalisedCoords);
		}

		break;
	case SDL_QUIT:
	{
		isFinished = true;
	}
	break;


	}

	}
}






