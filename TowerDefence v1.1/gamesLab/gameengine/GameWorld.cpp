#include "GameWorld.h"

GameWorld::GameWorld(AssetLib* assets)
{
	this->assets = assets;
}

void GameWorld::Draw(Camera& cam, glm::vec4 normalisedCoords, glm::mat4 depthMVP, float mouseX, float mouseY, ShadowMapFBO* smFBO, DirectionalLight dirlight)
{
	assets->lightingShader->Bind();
	assets->lightingShader->SetLightWVP(depthMVP);
	assets->lightingShader->SetWVP(cam.getProjectionMatrix() * cam.getView());
	assets->lightingShader->SetWorldMatrix(glm::mat4(1.0f));
	assets->lightingShader->SetMatSpecularPower(8);
	assets->lightingShader->SetMatSpecularIntensity(0.4);
	assets->lightingShader->SetEyeWorldPos(cam.getPos());
	assets->lightingShader->SetPointLights(pLights);
	assets->lightingShader->SetDirectionalLight(dirlight);

	glEnable(GL_TEXTURE_2D);
	smFBO->BindForReading(GL_TEXTURE1);
	assets->lightingShader->SetShadowMap(1);
	assets->lightingShader->SetTextureUnit(0);

	worldModel->BindModelsVAO();
	worldModel->RenderModel();
	assets->background->BindModelsVAO();
	assets->background->RenderModel();

	for (int i = 0; i < towers.size(); i++)
		towers[i]->Draw(cam, depthMVP);


	/********** BUILDING TOWER **********/
	if (waveDelay > 0 && gui->selectedTower != -1) {

		BuildTower = assets->towers[TowerType(gui->selectedTower)];
		Ray ray = Ray(mouseX, HEIGHT - mouseY, WIDTH, HEIGHT, &cam);
		glm::vec3 rayDir = ray.getRayDir();

		glm::vec3 camPos = cam.getPos();
		float modifier = (camPos.y - 5) / -rayDir.y;

		glm::vec3 worldCoords = glm::vec3(camPos.x + (rayDir.x * modifier), 5, camPos.z + (rayDir.z * modifier));
		buildCellPos.x = (int)(worldCoords.x + GRIDSIZE * (GRIDSIZE / 2)) / GRIDSIZE;
		buildCellPos.y = (int)(worldCoords.z + GRIDSIZE * (GRIDSIZE / 2)) / GRIDSIZE;
		if (buildCellPos.x >= 0 && buildCellPos.x < GRIDSIZE && buildCellPos.y >= 0 && buildCellPos.y < GRIDSIZE && map[(int)buildCellPos.y * GRIDSIZE + (int)buildCellPos.x] != 0) {
			buildWorldPos = glm::vec3(buildCellPos.x * GRIDSIZE - (GRIDSIZE*(GRIDSIZE / 2)) + (GRIDSIZE / 2), 5, buildCellPos.y * GRIDSIZE - (GRIDSIZE*(GRIDSIZE / 2)) + (GRIDSIZE / 2));
			assets->lightingShader->SetLightWVP(depthMVP * glm::translate(buildWorldPos));
			assets->lightingShader->SetWVP(cam.getProjectionMatrix() * cam.getView() * glm::translate(buildWorldPos));
			assets->lightingShader->SetWorldMatrix(glm::translate(buildWorldPos));
			BuildTower->BindModelsVAO();
			BuildTower->RenderModel();
		}
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	assets->lightingShader->Bind();
	assets->lightingShader->SetWVP(cam.getProjectionMatrix() * cam.getView());
	assets->lightingShader->SetLightWVP(depthMVP);
	assets->lightingShader->SetWorldMatrix(glm::mat4(1.0));
	grid->BindModelsVAO();
	grid->RenderModel();

	if (!waveFinished)
		activeWave->Draw(cam);

	//if (gui->activeTowerSelected)
	selectedTowerEffect->draw(cam);
	
	pathDrawer->Draw(cam);

	for (int i = 0; i < towers.size(); i++)
		towers[i]->DrawProjectiles(cam);

	for (int i = 0; i < effects.size(); i++)
	{
		effects[i]->draw(cam);
	}

	

	if (!gameOver) {

		string s0 = "Next Wave: " + to_string((int)waveDelay);
		string s1 = "Wave: " + to_string(waveCount);
		int kills = enemiesKilled;
		int finished = lives;//enemiesFinished;
		if (!waveFinished) {
			kills += activeWave->enemiesKilled;
			finished -= activeWave->enemiesFinished;
		}
		string s2 = "Kills: " + to_string(kills);
		string s3 = "Lives: " + to_string(finished);
		string s4 = "$" + to_string(resources);
		text->printString(s1.c_str(), 750, 850, 40, glm::vec3(1,1,1));
		text->printString(s2.c_str(), 1000, 850, 40, glm::vec3(1, 1, 1));
		text->printString(s3.c_str(), 1250, 850, 40, glm::vec3(1, (finished /10.0f), (finished /10.0f)));
		if (waveDelay > 0) {
			gui->Draw(normalisedCoords);
			text->printString(s0.c_str(), 400, 850, 40, glm::vec3(1, (waveDelay/30), (waveDelay / 30)));
			text->printString(s4.c_str(), 150, 775, 40, glm::vec3(1, 1, 1));
			text->printString("Press 'C' to skip", 40, 120, 20, glm::vec3(1, 1, 1));
			if (!gui->activeTowerSelected) {
				text->printString(("$" + to_string(Tower::costs[0])).c_str(), 100, 695, 30, glm::vec3(1, resources >= Tower::costs[0], resources >= Tower::costs[0]));
				text->printString(("$" + to_string(Tower::costs[1])).c_str(), 70, 550, 30, glm::vec3(1, resources >= Tower::costs[1], resources >= Tower::costs[1]));
				text->printString(("$" + to_string(Tower::costs[2])).c_str(), 100, 405, 30, glm::vec3(1, resources >= Tower::costs[2], resources >= Tower::costs[2]));
				text->printString(("$" + to_string(Tower::costs[3])).c_str(), 70, 260, 30, glm::vec3(1, resources >= Tower::costs[3], resources >= Tower::costs[3]));
			}
			else {
				text->printString(("Level: " + to_string(gui->targetTower->towerLevel)).c_str(), 100, 730, 30, glm::vec3(1, 1, 1));
				text->printString(("Damage: " + to_string((int)gui->targetTower->damage)).c_str(), 100, 680, 30, glm::vec3(1, 1, 1));
				text->printString(("Range: " + to_string((int)gui->targetTower->getRange())).c_str(), 100, 630, 30, glm::vec3(1, 1, 1));

				text->printString(("Sell(+$" + to_string(Tower::costs[gui->targetTower->getType()] / 2) + ")").c_str(), 80, 525, 30, glm::vec3(1, 1, 1));
				if (gui->targetTower->getType() != WALL)
					text->printString(("Upgrade($" + to_string(gui->targetTower->upgradeCost) + ")").c_str(), 80, 365, 30, glm::vec3(1, resources >= gui->targetTower->upgradeCost, resources >= gui->targetTower->upgradeCost));
			}
		}
	}
	else {

		text->printString("GAME OVER", 600, 500, 70, glm::vec3(1, 0, 0));
		string wavesComp = "You completed " + to_string(waveCount - 1) + " waves!";
		text->printString(wavesComp.c_str(), 500, 400, 40, glm::vec3(1, 1, 1));
		text->printString("Press 'Enter' to restart", 500, 300, 40, glm::vec3(1, 1, 1));
	}
}

void GameWorld::ShadowMapPass(glm::mat4 depthMVP)
{
	assets->shadowShader->Bind();
	assets->shadowShader->SetWVP(depthMVP);
	worldModel->BindModelsVAO();
	worldModel->RenderModel();
	grid->BindModelsVAO();
	grid->RenderModel();

	for (int i = 0; i < towers.size(); i++)
		towers[i]->ShadowMapPass(depthMVP);
	if(!waveFinished)
		activeWave->shadowMapPass(depthMVP);

	for (int i = 0; i < effects.size(); i++)
	{
		effects[i]->shadowMapPass(depthMVP);
	}

	pathDrawer->ShadowMapPass(depthMVP);
}

void GameWorld::Update(double deltaT, Camera& cam)
{
	if (!gameOver) {

		if (pLights.size() > 2)
			pLights.erase(pLights.begin() + 2, pLights.end());

		for (int i = 0; i < towers.size(); i++) {
			towers[i]->Update(deltaT, cam);
			PointLight p;
			switch (towers[i]->getType()) {

			case MORTAR:

				p.AmbientIntensity = 0.2;
				p.DiffuseIntensity = 0.9;
				p.Color = glm::vec3(4.0 / 255.0, 93.0 / 255.0, 28.0 / 255.0);
				p.Attenuation.Constant = 1;
				p.Attenuation.Linear = 0.0;
				p.Attenuation.Exp = 0.01;
				for each (ParticleSystem* proj in towers[i]->getProjectiles())
				{
					p.Position = proj->getSystemCenter();//towers[i]->getWorldPos() + glm::vec3(0, 20, 0);
					pLights.push_back(p);
				}

				break;
			case TURRET:
				p.AmbientIntensity = 0.2;
				p.DiffuseIntensity = 0.7;
				p.Color = glm::vec3(0.886, 0.345, 0.133);
				p.Attenuation.Constant = 1;
				p.Attenuation.Linear = 0.0;
				p.Attenuation.Exp = 0.02;
				for each (ParticleSystem* proj in towers[i]->getProjectiles())
				{
					p.Position = proj->getSystemCenter();//towers[i]->getWorldPos() + glm::vec3(0, 17, 0);
					pLights.push_back(p);
				}
				break;
			case FREEZE:
				p.AmbientIntensity = 0.7;
				p.DiffuseIntensity = 0.7;
				p.Color = glm::vec3(145.0 / 255.0, 1, 1);
				p.Attenuation.Constant = 1;
				p.Attenuation.Linear = 0.0;
				p.Attenuation.Exp = 0.05;
				p.Position = towers[i]->getWorldPos() + glm::vec3(0, 10, 0);
				pLights.push_back(p);

				break;
			}
			if (!waveFinished) {
				if (towers[i]->getTarget() == nullptr || !towers[i]->getTarget()->isAlive() || towers[i]->getTarget()->finished || glm::length(towers[i]->getWorldPos() - towers[i]->getTarget()->getWorldPos()) > towers[i]->getRange()) {
					for each (Enemy* e in activeWave->getActiveEnemys())
					{
						if (e != nullptr && e != towers[i]->getTarget()) {
							if (glm::length(e->getWorldPos() - towers[i]->getWorldPos()) <= towers[i]->getRange()) {
								towers[i]->setTarget(e);
								break;
							}
						}
					}
				}
			}
		}

		if (!waveFinished) {
			activeWave->Update(deltaT, cam);

			for each (Enemy* e in activeWave->getActiveEnemys()) {
				if (e != nullptr && e->deathTimer > 0) {
					PointLight p;
					p.AmbientIntensity = e->deathTimer / e->getParticlelife();
					p.DiffuseIntensity = e->deathTimer / e->getParticlelife();
					p.Color = e->lightColour;
					p.Position = e->getWorldPos();
					p.Attenuation.Constant = 1;
					p.Attenuation.Linear = 0.0;
					p.Attenuation.Exp = 0.05;
					pLights.push_back(p);
				}
			}
			if ((lives - activeWave->enemiesFinished) <= 0) {
				gameOver = true;
				lives = 0;
			}
		}


		if (!waveFinished && activeWave->waveFinished) {
			waveFinished = true;
			gui->resetGUI();
			selectedTowerEffect->setDuration(FLT_MAX);
			waveDelay = 30;
			waveCount++;
			enemiesFinished += activeWave->enemiesFinished;
			lives -= activeWave->enemiesFinished;
			if (lives <= 0)
				gameOver = true;
			enemiesKilled += activeWave->enemiesKilled;
			resources += activeWave->waveEarnings;
			for (int i = 0; i < towers.size(); i++)
			{
				towers[i]->setTarget(nullptr);
			}
			delete activeWave;
		}

		if (waveFinished) {
			waveDelay -= deltaT;
			if (waveDelay <= 0) {
				waveFinished = false;
				int numEnemies = 10 + (waveCount / 3);
				vector<EnemyType> enemies;
				for (int i = 0; i < numEnemies; i++)
					enemies.push_back(EnemyType(rand() % 4));
				activeWave = new Wave(enemies, assets, path, ((float)waveCount / 5)*((float)waveCount / 5));
				gui->resetGUI();
				selectedTowerEffect->setDuration(0);
			}
		}

		if (gui->activeTowerSelected)
			selectedTowerEffect->setDuration(FLT_MAX);
		else
			selectedTowerEffect->setDuration(0);
		selectedTowerEffect->update(cam, deltaT);

		for (int i = 0; i < effects.size(); i++)
		{
			if (effects[i] != nullptr) {
				effects[i]->update(cam, deltaT);
				if (effects[i]->finished) {
					delete effects[i];
					effects[i] = nullptr;
				}
			}
		}

		auto removeItt = remove_if(effects.begin(), effects.end(), [](AtmosphericEffect* e) {if (e == nullptr) return true; return false; });

		effects.erase(removeItt, effects.end());

		pathDrawer->Update(deltaT, cam);

		if (gui->sellButton->isSelected == 2) {
			auto removeItt = remove_if(towers.begin(), towers.end(), [=](Tower* t) {if (t == gui->targetTower) return true; return false; });
			towers.erase(removeItt, towers.end());
			gui->activeTowerSelected = false;
			selectedTowerEffect->setDuration(0);
			glm::vec2 cellPos = gui->targetTower->getCellPos();
			map[(int)cellPos.y * GRIDSIZE + (int)cellPos.x] = 1;

			AStar pathfinder = AStar(map, false, GRIDSIZE);
			path = pathfinder.search(glm::vec2(5, 0), glm::vec2(4, 9));
			pathDrawer->setPath(path);

			resources += Tower::costs[gui->targetTower->getType()] / 2;
			AtmosphericEffect* e = new AtmosphericEffect(assets, gui->targetTower->getWorldPos() + glm::vec3(0, 15, 0), RUBBLE);
			e->init();
			effects.push_back(e);
			delete gui->targetTower;
			gui->targetTower = nullptr;
			gui->sellButton->isSelected = 0;
			int chan = Mix_PlayChannel(-1, assets->destroyTower, 0);
			if (chan != -1) {
				float diff = MIX_MAX_VOLUME - ((glm::length(e->getWorldPos() - cam.getPos())));
				if (diff < 0)
					Mix_Volume(chan, 0);
				else
					Mix_Volume(chan, diff / 4);
			}
		}

		if (gui->upgradeButton->isSelected == 2) {
			gui->upgradeButton->isSelected = 0;
			if (resources >= gui->targetTower->upgradeCost) {
				resources -= gui->targetTower->upgradeCost;
				gui->targetTower->upgradeTower();
				AtmosphericEffect* e = new AtmosphericEffect(assets, gui->targetTower->getWorldPos() + glm::vec3(0, 20, 0), CONFETTI_EXPLOSION);
				e->init();
				effects.push_back(e);
				int chan = Mix_PlayChannel(-1, assets->upgradeTower, 0);
				if (chan != -1) {
					float diff = MIX_MAX_VOLUME - ((glm::length(e->getWorldPos() - cam.getPos())));
					if (diff < 0)
						Mix_Volume(chan, 0);
					else
						Mix_Volume(chan, diff / 4);
				}
			}
		}
	}
}

void GameWorld::Init()
{
	for (int x = 0; x < GRIDSIZE; x++) {
		for (int y = 0; y < GRIDSIZE; y++) {
			map[y*GRIDSIZE + x] = 1;
		}
	}

	AStar pathfinder = AStar(map, false, GRIDSIZE);
	path = pathfinder.search(glm::vec2(5, 0), glm::vec2(4, 9));

	gui = new GUI(assets->guiShader);
	gui->Init();

	pathDrawer = new PathDrawer(assets);
	pathDrawer->Init();
	pathDrawer->setPath(path);

	activeWave = new Wave(vector<EnemyType>{ EnemyType(rand() % 4), EnemyType(rand() % 4), EnemyType(rand() % 4), EnemyType(rand() % 4), EnemyType(rand() % 4)}, assets, path, 0.2);

	text = assets->font;
	text->init();

	selectedTowerEffect = new AtmosphericEffect(assets, glm::vec3(0, 0, 0), SELECTED_TOWER);
	selectedTowerEffect->init();
	selectedTowerEffect->finished = true;


	AtmosphericEffect* e = new AtmosphericEffect(assets, glm::vec3(-5, 10, 80), PORTAL_EMMISION);
	e->init();
	effects.push_back(e);
	e = new AtmosphericEffect(assets, glm::vec3(5, 10, -80), PORTAL_EMMISION);
	e->init();
	effects.push_back(e);
	e = new AtmosphericEffect(assets, glm::vec3(0, 5, 0), BUTTERFLYS);
	e->init();
	effects.push_back(e);
	worldModel = assets->world;
	BuildTower = assets->towers[0];
	grid = assets->grid;

	PointLight p;
	p.AmbientIntensity = 0.8;
	p.DiffuseIntensity = 0.8;
	p.Color = glm::vec3(0.56, 0.094, 0.7);
	p.Position = glm::vec3(-5, 10, 80);
	p.Attenuation.Constant = 1;
	p.Attenuation.Linear = 0.1;
	p.Attenuation.Exp = 0.01;

	pLights.push_back(p);

	p.Position = glm::vec3(5, 10, -80);

	pLights.push_back(p);

	waveDelay = 30;
	resources = 800;
	waveFinished = true;

}

void GameWorld::addTower() {
	if (waveDelay > 0 && resources >= Tower::costs[gui->selectedTower]) {
		if (buildCellPos.x >= 0 && buildCellPos.x < GRIDSIZE && buildCellPos.y >= 0 && buildCellPos.y < GRIDSIZE && map[(int)buildCellPos.y * GRIDSIZE + (int)buildCellPos.x] != 0 && (buildCellPos != glm::vec2(5,0))) {
			map[(int)buildCellPos.y*GRIDSIZE + (int)buildCellPos.x] = 0;
			AStar pathfinder = AStar(map, false, GRIDSIZE);
			vector<glm::vec2> newPath = pathfinder.search(glm::vec2(5, 0), glm::vec2(4, 9));
			if (newPath.size() < 9) {
				map[(int)buildCellPos.y*GRIDSIZE + (int)buildCellPos.x] = 1;
			}
			else {
				path = newPath;
				pathDrawer->setPath(path);
				Tower* t = new Tower(TowerType(gui->selectedTower), assets, buildCellPos);
				t->Init();
				AtmosphericEffect* e = new AtmosphericEffect(assets, t->getWorldPos() + glm::vec3(0, 22, 0), TOWER_BUILD_SMOKE);
				e->init();
				effects.push_back(e);
				towers.push_back(t);
				resources -= Tower::costs[gui->selectedTower];
				int chan = Mix_PlayChannel(-1, assets->buildTower, 0);
				if (chan != -1) {
					Mix_Volume(chan, MIX_MAX_VOLUME / 4);
				}
			}
		}
	}

}

void GameWorld::selectTower(float mouseX, float mouseY, Camera& cam)
{
	Ray ray = Ray(mouseX, HEIGHT - mouseY, WIDTH, HEIGHT, &cam);
	glm::vec3 rayDir = ray.getRayDir();

	glm::vec3 camPos = cam.getPos();
	float modifier = (camPos.y - 5) / -rayDir.y;

	glm::vec3 worldCoords = glm::vec3(camPos.x + (rayDir.x * modifier), 5, camPos.z + (rayDir.z * modifier));
	glm::vec2 cellPos;
	cellPos.x = (int)(worldCoords.x + GRIDSIZE * (GRIDSIZE / 2)) / GRIDSIZE;
	cellPos.y = (int)(worldCoords.z + GRIDSIZE * (GRIDSIZE / 2)) / GRIDSIZE;

	if (map[(int)cellPos.y * GRIDSIZE + (int)cellPos.x] == 0) {
		for each (Tower* t in towers)
		{
			if (t->getCellPos() == cellPos) {
				gui->targetTower = t;
				gui->activeTowerSelected = true;
				selectedTowerEffect->setWorldPos(t->getWorldPos());
				selectedTowerEffect->resetDeathTimer();
				selectedTowerEffect->setDuration(FLT_MAX);
				selectedTowerEffect->finished = false;
				return;
			}
		}
	}

	gui->targetTower = nullptr;
	gui->activeTowerSelected = false;
	selectedTowerEffect->setDuration(0);
	selectedTowerEffect->resetDeathTimer();
}

void GameWorld::resetWorld()
{
	gameOver = false;
	for each (Tower* t in towers)
	{
		Tower* tower = t;
		t = nullptr;
		delete tower;
	}
	towers.clear();

	for (int i = 0; i < GRIDSIZE*GRIDSIZE; i++)
	{
		map[i] = 1;
	}

	AStar pathfinder = AStar(map, false, GRIDSIZE);
	path = pathfinder.search(glm::vec2(5, 0), glm::vec2(4, 9));

	pathDrawer->setPath(path);
	activeWave = new Wave(vector<EnemyType>{ EnemyType(rand() % 4), EnemyType(rand() % 4), EnemyType(rand() % 4), EnemyType(rand() % 4), EnemyType(rand() % 4)}, assets, path, 0.2);

	lives = 10;
	enemiesKilled = 0;
	enemiesFinished = 0;
	waveCount = 1;
	waveDelay = 30;
	resources = 800;
	waveFinished = true;
	gui->resetGUI();

}


