#include "Enemy.h"

Enemy::Enemy(EnemyType type, AssetLib* assets, vector<glm::vec2> path, float statMod)
{
	this->type = type;
	worldPos = glm::vec3(5, 15, -80);
	float gridSize = GRIDSIZE;
	this->path = vector<glm::vec2>();
	this->path.push_back(glm::vec2(-5, 80));
	glm::vec2 p0, p1, p2, p3;
	for (int i = 0; i < path.size() - 1; i++) {
		if (i == 0)
			p0 = glm::vec2(-5, 80);
		else
			p0 = path[i - 1] * gridSize - (gridSize*(gridSize / 2)) + (gridSize / 2);

		p1 = path[i] * gridSize - (gridSize*(gridSize / 2)) + (gridSize / 2);
		p2 = path[i + 1] * gridSize - (gridSize*(gridSize / 2)) + (gridSize / 2);

		if (i == path.size() - 2)
			p3 = glm::vec2(5, -80);
		else
			p3 = path[i + 2] * gridSize - (gridSize*(gridSize /2)) + (gridSize /2);

		CatmullRomSpline x = CatmullRomSpline(p0.x, p1.x, p2.x, p3.x);
		CatmullRomSpline y = CatmullRomSpline(p0.y, p1.y, p2.y, p3.y);

		for (int j = 0; j < 5; j++) {
			float t = j * 0.2;	
			this->path.push_back(glm::vec2(x.PointAlongLine(t), y.PointAlongLine(t)));
		}
	}
	this->path.push_back(glm::vec2(5, -80));

	finished = false;
	switch (type) {
	case SLOW:
		health = 1200 * statMod;
		value = 100;
		speed = 2;
		baseSpeed = 2;
		if (rand() % 2) {
			particleSystem = new ParticleSystem(ParticleSystem::DirSmallStatic, ParticleSystem::OrbitZInwardXZ, glm::vec3(-25, 8, 25), assets->particleShader, assets->particleTextures[1]);
			lightColour = glm::vec3(0.886, 0.345, 0.133);//orange
		}
		else {
			particleSystem = new ParticleSystem(ParticleSystem::DirSmallStatic, ParticleSystem::OrbitZInwardXZ, glm::vec3(-25, 8, 25), assets->particleShader, assets->particleTextures[13]);
			lightColour = glm::vec3(183.0 / 255, 25.0 / 255, 173.0 / 255);//pink
		}
		break;
	case NORMAL:
		health = 900 * statMod;
		value = 100;
		speed = 4;
		baseSpeed = 4;
		if (rand() % 2) {
			particleSystem = new ParticleSystem(ParticleSystem::GravSmallGrow, ParticleSystem::OrbitYBackward, glm::vec3(25, 10, -25), assets->particleShader, assets->particleTextures[2]);
			lightColour = glm::vec3(0.886, 0.345, 0.133);//orange
		}
		else {
			particleSystem = new ParticleSystem(ParticleSystem::GravSmallGrow, ParticleSystem::OrbitYBackward, glm::vec3(25, 10, -25), assets->particleShader, assets->particleTextures[12]);
			lightColour = glm::vec3(0, 215.0 / 255, 1);//blue
		}
		break;
	case FAST:
		health = 600 * statMod;
		value = 100;
		speed = 8;
		baseSpeed = 8;
		if (rand() % 2) {
			particleSystem = new ParticleSystem(ParticleSystem::DirMedShrink, ParticleSystem::OrbitXOut, glm::vec3(-25, 10, -25), assets->particleShader, assets->particleTextures[3]);
			lightColour = glm::vec3(0.886, 0.345, 0.133);//orange
		}
		else {
			particleSystem = new ParticleSystem(ParticleSystem::DirMedShrink, ParticleSystem::OrbitXOut, glm::vec3(-25, 10, -25), assets->particleShader, assets->particleTextures[14]);
			lightColour = glm::vec3(31.0 / 255, 233.0 / 255, 77.0 / 255);//green
		}
		break;
	case BOSS:
		health = 1800 * statMod;
		value = 200;
		speed = 4;
		baseSpeed = 4;
		if (rand() % 2) {
			particleSystem = new ParticleSystem(ParticleSystem::GravDirMedStatic, ParticleSystem::BelowForcedXZPulse, glm::vec3(25, 10, 25), assets->particleShader, assets->particleTextures[0]);
			lightColour = glm::vec3(0.886, 0.345, 0.133);//orange
		}
		else {
			particleSystem = new ParticleSystem(ParticleSystem::GravDirMedStatic, ParticleSystem::BelowForcedXZPulse, glm::vec3(25, 10, 25), assets->particleShader, assets->particleTextures[12]);
			lightColour = glm::vec3(0, 215.0 / 255, 1);//blue
		}
		break;
	}
	deathTimer = particleSystem->getMaxParticleLife();
	shadowShader = assets->particleShadowShader;
	slowedTimer = 0;
}

void Enemy::Init()
{
	particleSystem->init();
}

void Enemy::Update(double deltaT, Camera& cam)
{
	slowedTimer -= deltaT;
	if (slowedTimer <= 0)
		speed = baseSpeed;

	if (path.size() > 0) {

		glm::vec3 targetPos = glm::vec3(path.back().x, 15, path.back().y);

		if (glm::length(targetPos - worldPos) > speed*deltaT) {
			glm::vec3 moveVec = glm::normalize(targetPos - worldPos);
			worldPos += moveVec * (float)(speed * deltaT);
		}
		else {
			worldPos = targetPos;
			path.pop_back();
		}

	}
	else {
		finished = true;
	}

	if (finished || !isAlive()) {
		particleSystem->setSystemCenter(glm::vec3(1000,-1000,1000));
		deathTimer -= deltaT;
	}
	else {
		particleSystem->setSystemCenter(worldPos);
	}
	particleSystem->update(deltaT, cam);

	
}

void Enemy::Draw(Camera& cam)
{
	particleSystem->draw(cam);
}

void Enemy::ShadowPass(glm::mat4 depthMVP)
{
	shadowShader->Bind();
	shadowShader->SetWVP(depthMVP);
	particleSystem->shadowMapPass(shadowShader);
}

void Enemy::SlowEnemy()
{
	slowedTimer = 1;
	speed = baseSpeed / 2;
}
