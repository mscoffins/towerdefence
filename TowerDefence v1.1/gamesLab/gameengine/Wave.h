#pragma once
#include "Enemy.h"
#include "AssetLib.h"

class Wave{
private:
	float spawnRate;
	float timeSinceSpawn;
	
	vector<Enemy*> enemys;
	vector<Enemy*> activeEnemies;
	
public:
	Wave() {}
	Wave(vector<EnemyType> enemys, AssetLib* assets, vector<glm::vec2> path, float statMod = 1);
	void Update(double deltaT, Camera& cam);
	void Draw(Camera& cam);
	void shadowMapPass(glm::mat4 depthMVP);
	vector<Enemy*> getActiveEnemys() { return activeEnemies; }
	
	bool waveFinished;
	int enemiesKilled;
	int enemiesFinished;
	int waveEarnings;

	~Wave() {
		for each (Enemy* e in enemys)
		{
			Enemy* del = e;
			e = nullptr;
			if(del != nullptr)
				delete del;
		}
		for each (Enemy* e in activeEnemies)
		{
			Enemy* del = e;
			e = nullptr;
			if (del != nullptr)
				delete del;
		}
	}
};