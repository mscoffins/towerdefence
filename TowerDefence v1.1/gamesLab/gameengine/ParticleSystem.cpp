#include "ParticleSystem.h"
#include <math.h>

ParticleProperties ParticleSystem::GravDirMedStatic = ParticleProperties(true, 1.5, 6, true, 2.6, 1.1, STATIC,5);
ParticleProperties ParticleSystem::DirSmallStatic = ParticleProperties(false, 1, 3, true, 4.0, 3.0, STATIC,15);
ParticleProperties ParticleSystem::GravSmallGrow = ParticleProperties(true, 1, 3, false, 2.5, 1.5, GROW,10);
ParticleProperties ParticleSystem::DirMedShrink = ParticleProperties(false, 0.5, 2, true, 4.6, 2.1, SHRINK,10);
ParticleProperties ParticleSystem::TurretFireBall = ParticleProperties(false, 0.7, 1, false, 2, 1.5, SHRINK,6);
ParticleProperties ParticleSystem::MotarCanonBall = ParticleProperties(false, 1, 1, false, 3, 2.5, SHRINK,10);
ParticleProperties ParticleSystem::FreezeElectricBolt = ParticleProperties(false, 0.5, 2, false, 1, 0.9, STATIC,4);
ParticleProperties ParticleSystem::PathParticles = ParticleProperties(false, 0, 11, false, 2.1, 2, STATIC, 5);
ParticleProperties ParticleSystem::TowerSmoke = ParticleProperties(false, 1, 3, true, 4, 2, GROW, 4);
ParticleProperties ParticleSystem::PortalParticles = ParticleProperties(false, 3, 3, false, 1.5, 1.0, STATIC, 3);
ParticleProperties ParticleSystem::Butterflys = ParticleProperties(false, 5, 20, true, 1.5, 1.0, SHRINK, 1);
ParticleProperties ParticleSystem::RubbleParticles = ParticleProperties(true, 1, 4, true, 3, 1.5, STATIC, 1);
ParticleProperties ParticleSystem::SelectParticles = ParticleProperties(false, 1, 2, true, 2, 1, STATIC, 2);
ParticleProperties ParticleSystem::Confetti = ParticleProperties(true,0,1,true,2,1,SHRINK,2);

EmmiterProperties ParticleSystem::BelowForcedXZPulse = EmmiterProperties(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0), 0, false, ABOVE, 5, glm::vec3(1, 0.1, 1), 0,glm::vec3(0,0,0) ,500);
EmmiterProperties ParticleSystem::OrbitZInwardXZ = EmmiterProperties(glm::vec3(0, 3, 0), glm::vec3(0, 0, 1), 180, true, INWARDS, 1, glm::vec3(3, 1, 3), 0, glm::vec3(0, 0, 0), 200);
EmmiterProperties ParticleSystem::OrbitYBackward = EmmiterProperties(glm::vec3(0, 0, 2), glm::vec3(0, 1, 0), 360, false, BACKWARDS, 2, glm::vec3(1, 1, 1), 0, glm::vec3(0, 0, 0), 800);
EmmiterProperties ParticleSystem::OrbitXOut = EmmiterProperties(glm::vec3(0, 1, 0), glm::vec3(1, 0, 0), 270, true, OUTWARDS, 2, glm::vec3(1, 1, 1), 0, glm::vec3(0, 0, 0), 400);
EmmiterProperties ParticleSystem::TurretFireBallEmmit = EmmiterProperties(glm::vec3(0.5, 0, 0), glm::vec3(1, 0, 0), 720, true, EmmisionDirection::BACKWARDS, 1, glm::vec3(1, 1, 1), 0, glm::vec3(0, 0, 0), 150);
EmmiterProperties ParticleSystem::MotarCanonBallEmmit = EmmiterProperties(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 720, false, EmmisionDirection::BACKWARDS, 1, glm::vec3(1, 1, 1), 0, glm::vec3(0, 0, 0), 200);
EmmiterProperties ParticleSystem::FreezeElectricBoltEmmit = EmmiterProperties(glm::vec3(0.5, 0, 0), glm::vec3(1, 0, 0), 720, true, EmmisionDirection::FORWARD, 10, glm::vec3(1, 1, 1), 0, glm::vec3(0, 0, 0), 200);
EmmiterProperties ParticleSystem::PathEmmiter = EmmiterProperties(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 0, false, EmmisionDirection::BACKWARDS, 1, glm::vec3(1, 1, 1), 0, glm::vec3(0, 0, 0), 1000);
EmmiterProperties ParticleSystem::TowerSmokeEmmit = EmmiterProperties(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 0, false, EmmisionDirection::ABOVE, 3, glm::vec3(2, 0, 2), 0, glm::vec3(3.5, 0, 3.5), 100);
EmmiterProperties ParticleSystem::PortalEmmit = EmmiterProperties(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 0, false, EmmisionDirection::BACKWARDS, 1, glm::vec3(1, 2, 1), 0, glm::vec3(15, 0, 5), 100);
EmmiterProperties ParticleSystem::ButterflyEmmit = EmmiterProperties(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 0, false, EmmisionDirection::ABOVE, 5, glm::vec3(1, 5, 1), 0, glm::vec3(50, 0, 50), 20);
EmmiterProperties ParticleSystem::RubbleEmmit = EmmiterProperties(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 0, false, EmmisionDirection::BELOW, 5, glm::vec3(2, 0, 2), 0, glm::vec3(3, 0, 3), 100);
EmmiterProperties ParticleSystem::SelectEmmit = EmmiterProperties(glm::vec3(3, 0, 0), glm::vec3(0, 1, 0), 720, false, EmmisionDirection::ABOVE, 50, glm::vec3(0.1, 0,0.1), 0, glm::vec3(0, 0, 0), 300);
EmmiterProperties ParticleSystem::ExplosionEmmit = EmmiterProperties(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 0, false, EmmisionDirection::RANDOM, 10, glm::vec3(1, 1, 1), 0, glm::vec3(1, 1, 1), 500);

ParticleSystem::ParticleSystem()
{
	pProps = ParticleProperties();
	eProps = EmmiterProperties();
	systemCentre = glm::vec3(0, 5, 0);
}

ParticleSystem::ParticleSystem(ParticleProperties pp, EmmiterProperties ep, glm::vec3 sc, ParticleShader* ps, pair<Texture*, glm::vec3> tex)
{
	pProps = pp;
	eProps = ep;
	systemCentre = sc;
	shader = ps;
	texture = tex;
}

int ParticleSystem::FindUnusedParticle()
{
	for (int i = lastParticleUsed; i<eProps.maxParticles; i++) {
		if (particles[i].life < 0) {
			lastParticleUsed = i;
			return i;
		}
	}
	
	for (int i = 0; i<lastParticleUsed; i++) {
		if (particles[i].life < 0) {
			lastParticleUsed = i;
			return i;
		}
	}

	return 0; // All particles are taken, override the first one
}

void ParticleSystem::SortParticles()
{
	std::sort(&particles[0], &particles[eProps.maxParticles-1]);
}

void ParticleSystem::init()
{

	emitterPos = systemCentre;
	alive = true;
	moveTime = 0;
	pulseTimer = 0;
	lastSpawnTimer = 0;
	particleCount = 0;
	particles = vector<Particle>(eProps.maxParticles);
	g_positionData = new GLfloat[eProps.maxParticles * 4];
	g_colourData = new GLubyte[eProps.maxParticles * 4];
	g_lifeData = new GLfloat[eProps.maxParticles];
	for (int i = 0; i<eProps.maxParticles; i++) {
		particles[i].life = -1.0f;
		particles[i].cameradistance = -1.0f;
	}

	glGenVertexArrays(1, &VAO);
	glGenBuffers(3, buffers);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

	glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (const GLvoid*)0);
	
	glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	

	glBindVertexArray(0);


}

void ParticleSystem::update(double deltaT, Camera& camera)
{
	lastSpawnTimer += deltaT;

	int newparticles = 0;
	if (eProps.pulseRate > 0) {
		pulseTimer += deltaT;
		if (pulseTimer < pProps.maxLife / (2 * eProps.pulseRate)) {
			newparticles = (int)(lastSpawnTimer*(eProps.maxParticles / (pProps.maxLife*0.5)));
		}
		else {
			if (pulseTimer > pProps.maxLife / eProps.pulseRate)
				pulseTimer = 0;
		}
	}
	else {
		newparticles = (int)(lastSpawnTimer*(eProps.maxParticles / pProps.maxLife));
	}
	

	if (particleCount >= eProps.maxParticles)
		newparticles = 0;

	if (newparticles > 0)
		lastSpawnTimer = 0;

	moveTime += deltaT * eProps.rotationSpeed;
	if (moveTime > 360)
		moveTime -= 360;
	glm::vec3 prevpos = emitterPos;
	glm::mat4 rot = glm::rotate((float)moveTime, eProps.rotationAxis);
	glm::vec3 axis = glm::vec3(1, 0, 1);

	glm::quat qRot = glm::rotate(glm::quat(), moveTime, eProps.rotationAxis);

	glm::vec4 pos = glm::mat4_cast(qRot) * glm::vec4(eProps.centerOffset, 0);
	emitterPos = systemCentre + glm::vec3(pos);

	glm::vec3 emitDir;

	switch (eProps.dir) {
	case FORWARD:
		if ( prevpos - emitterPos != glm::vec3(0, 0, 0))
			emitDir = glm::normalize(prevpos - emitterPos);
		else
			emitDir = -emitterPos;
		break;
	case BACKWARDS:
		if (emitterPos - prevpos != glm::vec3(0, 0, 0))
			emitDir = glm::normalize(emitterPos - prevpos);
		else
			emitDir = -emitterPos;
		break;
	case INWARDS:
		if (systemCentre - emitterPos != glm::vec3(0, 0, 0))
			emitDir = glm::normalize(systemCentre - emitterPos);
		else
			emitDir = glm::vec3(0, 0, 0);
		break;
	case OUTWARDS:
		if (emitterPos - systemCentre != glm::vec3(0, 0, 0))
			emitDir = glm::normalize(emitterPos - systemCentre);
		else
			emitDir = glm::vec3(0, 0, 0);
		break;
	case ABOVE:
		emitDir = glm::vec3(0, 1, 0);
		break;
	case BELOW:
		emitDir = glm::vec3(0, -1, 0);
		break;
	case RANDOM:
		emitDir = glm::normalize(glm::vec3(
			((rand() % 2000 - 1000.0f) / 1000.0f),
			((rand() % 2000 - 1000.0f) / 1000.0f),
			((rand() % 2000 - 1000.0f) / 1000.0f)
			));
	}

	for (int i = 0; i<newparticles; i++) {
		int particleIndex = FindUnusedParticle();
		particles[particleIndex].life = pProps.maxLife; 

		glm::vec3 randomSpread = glm::vec3(
			((rand() % 2000 - 1000.0f) / 1000.0f),
			((rand() % 2000 - 1000.0f) / 1000.0f),
			((rand() % 2000 - 1000.0f) / 1000.0f)
		);
		randomSpread = eProps.spawnSpread * glm::normalize(randomSpread);
		particles[particleIndex].pos = emitterPos + randomSpread;//system origin

		glm::vec3 maindir = emitDir;//start velocity

		glm::vec3 randomdir = glm::vec3(
			eProps.axisInfulence.x * ((rand() % 2000 - 1000.0f) / 1000.0f),
			eProps.axisInfulence.y * ((rand() % 2000 - 1000.0f) / 1000.0f),
			eProps.axisInfulence.z * ((rand() % 2000 - 1000.0f) / 1000.0f)
		);
		randomdir = glm::normalize(randomdir);
		if(pProps.directed)
			particles[particleIndex].speed = maindir * eProps.emmitionForce + randomdir*pProps.spread;
		else
			particles[particleIndex].speed = randomdir*pProps.spread;

		// Very bad way to generate a random color
		particles[particleIndex].r = rand() % 256;
		particles[particleIndex].g = rand() % 256;
		particles[particleIndex].b = rand() % 256;
		particles[particleIndex].a = 1;

		particles[particleIndex].size = (rand() % ((int)(1000 * (pProps.maxSize - pProps.minSize)))) / 1000.0f + pProps.minSize;

	}

	vector<thread> threads;
	// Simulate all particles
	for (int i = 0; i<eProps.maxParticles; i++) {

		Particle& p = particles[i]; // shortcut
		
		if (p.life > 0.0f) {

			// Decrease life
			p.life -= deltaT;
			if (p.life > 0.0f) {

				// Simulate simple physics : gravity only, no collisions
				if (pProps.gravity)
					p.speed += glm::vec3(0, -9.81, 0) * (float)deltaT;
				p.pos += p.speed * (float)deltaT;
				if (p.pos.y < 5 + p.size) {
					p.pos.y = 5 + p.size;
					p.speed.y = -(p.speed.y * 0.5);
				}
				
				p.cameradistance = glm::dot(p.pos - camera.getPos(), p.pos - camera.getPos());;

			}
			else {
				// Particles that just died will be put at the end of the buffer in SortParticles();
				p.cameradistance = -1.0f;
			}

		}
	}

	//cout << deltaT << endl;
	if(pProps.alphaFactor == 1)
		SortParticles();

	particleCount = 0;
	for (int i = 0; i < eProps.maxParticles; i++) {

		Particle& p = particles[i];

		if (p.life > 0) {
			g_positionData[4 * particleCount + 0] = p.pos.x;
			g_positionData[4 * particleCount + 1] = p.pos.y;
			g_positionData[4 * particleCount + 2] = p.pos.z;

			switch (pProps.scaleType) {
			case STATIC:
				g_positionData[4 * particleCount + 3] = p.size;
				break;
			case GROW:
				g_positionData[4 * particleCount + 3] = p.size + p.size * ((pProps.maxLife - p.life) / pProps.maxLife);
				break;
			case SHRINK:
				g_positionData[4 * particleCount + 3] = p.size - p.size * ((pProps.maxLife - p.life) / pProps.maxLife);
				break;
			}

			g_colourData[4 * particleCount + 0] = p.r / p.life;
			g_colourData[4 * particleCount + 1] = p.g / p.life;
			g_colourData[4 * particleCount + 2] = p.b / p.life;
			g_colourData[4 * particleCount + 3] = p.a;

			g_lifeData[particleCount] = p.life;

			particleCount++;
		}

	}

	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
	glBufferData(GL_ARRAY_BUFFER, particleCount * 4 * sizeof(GLfloat), g_positionData, GL_DYNAMIC_DRAW); 
	glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
	glBufferData(GL_ARRAY_BUFFER, particleCount * 4 * sizeof(GLubyte), g_colourData, GL_DYNAMIC_DRAW); 
	glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
	glBufferData(GL_ARRAY_BUFFER, particleCount * sizeof(GLfloat), g_lifeData, GL_DYNAMIC_DRAW);
}

void ParticleSystem::draw(Camera& cam)
{
	glEnable(GL_BLEND);
	if (pProps.alphaFactor != 1) {
		glDepthMask(GL_FALSE);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	}else
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
	glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
	shader->Bind();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.first->texture);
	shader->Update(cam, pProps.maxLife,texture.second, pProps.alphaFactor);
	

	glBindVertexArray(VAO);
	glPointSize(5);
	glDrawArrays(GL_POINTS, 0, particleCount);

	glBindVertexArray(0);
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_POINT_SPRITE);
	glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);
}

void ParticleSystem::shadowMapPass(ShadowMapParticleShader* smps)
{
	smps->setMaxLife(pProps.maxLife);
	smps->setSpriteSheetDimmentions(texture.second);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.first->texture);
	smps->setTextureLocation(0);
	glEnable(GL_POINT_SPRITE);
	glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

	glBindVertexArray(VAO);
	glPointSize(5);
	glDrawArrays(GL_POINTS, 0, particleCount);
	glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);
}
