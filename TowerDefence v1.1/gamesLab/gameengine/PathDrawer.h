#pragma once
#include "DefinedVars.h"
#include "AssetLib.h"
#include "ParticleSystem.h"
#include "CatmullRomSpline.h"

class PathDrawer {
private:
	ParticleSystem* pathLine;
	vector<glm::vec2> path;
	vector<glm::vec2> navigatingPath;
	ShadowMapParticleShader* shadowShader;

public:
	PathDrawer() {}
	PathDrawer(AssetLib* assets);

	void setPath(vector<glm::vec2> path);
	void UpdatePath(vector<glm::vec2> path) { this->path = path; }
	void Init();
	void Draw(Camera& cam);
	void Update(double deltaT, Camera& cam);
	void ShadowMapPass(glm::mat4 depthMVP);
};