#include "Menu.h"

Menu::Menu(GuiShader * shader)
{
	this->verts = new Vert[4]{
		Vert(glm::vec3(-1.0,-1.0,0), glm::vec2(0.0,1.0), glm::vec3(0.0,0.0,-1.0)),
		Vert(glm::vec3(1.0,-1.0,0), glm::vec2(1.0,1.0), glm::vec3(0.0,0.0,-1.0)),
		Vert(glm::vec3(1.0,1.0,0), glm::vec2(1.0,0.0), glm::vec3(0.0,0.0,-1.0)),
		Vert(glm::vec3(-1.0,1.0,0), glm::vec2(0.0,0.0), glm::vec3(0.0,0.0,-1.0))

	};

	unsigned int indices[] = { 0,1,2,0,2,3 };
	this->background = new Mesh(verts, sizeof(verts), indices, sizeof(indices));
	this->controls = new Mesh(verts, sizeof(verts), indices, sizeof(indices));
	this->shader = shader;
	showControls = false;
	playGame = false;
}

void Menu::Init()
{
	this->textureBG = new Texture("assets/titleScreen.png");
	this->textureControls = new Texture("assets/howTo.png");

	string playBtnName = "assets/textures/playButton.png";
	string howToButtonName = "assets/textures/howtoButton.png";

	float	xMin = -0.175,
			yMin = -0.85,
			xMax = 0.175,
			yMax = -0.6;

	glm::vec3 buttonPos((xMin + xMax) / 2, (yMin + yMax) / 2, 0);
	glm::vec2 buttonDim(xMax - xMin, yMax - yMin);
	Texture* text = new Texture(playBtnName);
	Button* butt = new Button(buttonPos, buttonDim, text, this->shader);
	playBtn = butt;

	xMin = -0.175;
	yMin = -0.55;
	xMax = 0.175;
	yMax = -0.3;

	glm::vec3 buttonPos2((xMin + xMax) / 2, (yMin + yMax) / 2, 0);
	glm::vec2 buttonDim2(xMax - xMin, yMax - yMin);
	Texture* text2 = new Texture(howToButtonName);
	Button* butt2 = new Button(buttonPos2, buttonDim2, text2, this->shader);
	controlsBtn = butt2;

}

void Menu::buttonCollision(glm::vec4 normalisedCoords)
{

	if (playBtn->checkCollision(normalisedCoords)) {
		playBtn->isSelected = 2;
		playGame = true;
	}

	if (!showControls) {
		if (controlsBtn->checkCollision(normalisedCoords)) {
			controlsBtn->isSelected = 2;
			showControls = true;
		}
	}
	else {
		showControls = false;
	}

}

void Menu::Draw(glm::vec4 normalisedCoords)
{

	// Enable blending and alpha
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	this->shader->Bind();
	this->shader->setTextureLocation(0);
	this->shader->setIsSelected(0);

	//Draw GUI background
	textureBG->Bind(0);
	glEnable(GL_TEXTURE_2D);
	this->background->Draw();

	//draw controls window
	if (showControls) {
		textureControls->Bind(0);
		this->controls->Draw();
	}
	else {
		//draw controls button
		if (controlsBtn->isSelected == 2) {
			controlsBtn->Draw();
			controlsBtn->isSelected = 0;
		}
		//if not selected check to see if hovering over if so then set
		else if (controlsBtn->checkCollision(normalisedCoords))
		{
			controlsBtn->isSelected = 1;
			controlsBtn->Draw();
		}
		//if not hovering over and not selected then set to 0
		else
		{
			controlsBtn->isSelected = 0;
			controlsBtn->Draw();
		}
	}

	//draw play button
	if (playBtn->isSelected == 2) {
		playBtn->Draw();
		playBtn->isSelected = 0;
	}
	//if not selected check to see if hovering over if so then set
	else if (playBtn->checkCollision(normalisedCoords))
	{
		playBtn->isSelected = 1;
		playBtn->Draw();
	}
	//if not hovering over and not selected then set to 0
	else
	{
		playBtn->isSelected = 0;
		playBtn->Draw();
	}

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

}
