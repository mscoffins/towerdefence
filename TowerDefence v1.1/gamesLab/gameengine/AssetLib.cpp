#include "AssetLib.h"

void AssetLib::LoadAssets()
{
	font = new Text2D("assets/textures/font.png", 0.547);
	font->init();
	lightingShader = new LightingShader("Shader/Shaders/lighting");
	shadowShader = new ShadowMapShader("Shader/Shaders/ShadowMap");
	particleShadowShader = new ShadowMapParticleShader("Shader/Shaders/ShadowMapParticle");
	particleShader = new ParticleShader("Shader/Shaders/Particle");
	guiShader = new GuiShader("Shader/Shaders/GuiShader");

	world = new AssimpModel();
	world->LoadModelFromFile("assets/models/world.obj");
	world->FinalizeVBO();
	background = new AssimpModel();
	background->LoadModelFromFile("assets/models/background.obj");
	background->FinalizeVBO();
	AssimpModel* temp;
	temp = new AssimpModel();
	temp->LoadModelFromFile("assets/models/TowerWall.obj");
	temp->FinalizeVBO();
	towers.push_back(temp);
	temp = new AssimpModel();
	temp->LoadModelFromFile("assets/models/TowerT1.obj");
	temp->FinalizeVBO();
	towers.push_back(temp);
	temp = new AssimpModel();
	temp->LoadModelFromFile("assets/models/TowerSlow.obj");
	temp->FinalizeVBO();
	towers.push_back(temp);
	temp = new AssimpModel();
	temp->LoadModelFromFile("assets/models/TowerMortar.obj");
	temp->FinalizeVBO();
	towers.push_back(temp);

	pair<Texture*, glm::vec3> pTex;
	pTex = pair<Texture*,glm::vec3>(new Texture("assets/textures/ParticleSheet9974.png"), glm::vec3(9,9,74));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/ParticleSheet7749.png"), glm::vec3(7, 7, 49));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/ParticleSheet10548.png"), glm::vec3(10, 5, 48));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/ParticleSheet.png"), glm::vec3(8, 8, 64));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/FireSmoke6530.png"), glm::vec3(6, 5, 30));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/ParticleSheet6636.png"), glm::vec3(6, 6, 36));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/Energy_Ball5420.png"), glm::vec3(5, 4, 20));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/MortarBall9654.png"), glm::vec3(9, 6, 54));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/bSmoke6530.png"), glm::vec3(6, 5, 30));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/FireDebris10440.png"), glm::vec3(10, 4, 40));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/portalExplotion8432.png"), glm::vec3(8, 4, 32));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/butterfly13687.png"), glm::vec3(13, 7, 87));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/blue9974.png"), glm::vec3(9, 9, 74));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/pink7749.png"), glm::vec3(7, 7, 49));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/green10548.png"), glm::vec3(10, 5, 48));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/red8864.png"), glm::vec3(8, 8, 64));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/rocks8864.png"), glm::vec3(8, 8, 64));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/sparkles4416.png"), glm::vec3(4, 4, 16));
	particleTextures.push_back(pTex);
	pTex = pair<Texture*, glm::vec3>(new Texture("assets/textures/confetti.png"), glm::vec3(1, 1, 1));
	particleTextures.push_back(pTex);

	grid = new AssimpModel();
	grid->LoadModelFromFile("assets/models/grid.obj");
	grid->FinalizeVBO();

	bgMusic = Mix_LoadMUS("assets/Audio/bgtheme.wav");
	if (bgMusic == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
	turretFire = Mix_LoadWAV("assets/Audio/248116__robinhood76__05224-fireball-whoosh.wav");
	if (turretFire == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
	freezeFire = Mix_LoadWAV("assets/Audio/zap.wav");
	if (freezeFire == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
	mortarFire = Mix_LoadWAV("assets/Audio/mortar.wav");
	if (mortarFire == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
	buildTower = Mix_LoadWAV("assets/Audio/build.wav");
	if (buildTower == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
	destroyTower = Mix_LoadWAV("assets/Audio/rubble.wav");
	if (destroyTower == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
	upgradeTower = Mix_LoadWAV("assets/Audio/upgrade.wav");
	if (upgradeTower == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
	}
}
