#include "FontShader.h"

FontShader::FontShader(const std::string & filename):Shader(filename)
{
	m_textureLocation = glGetUniformLocation(main_program, "gSampler");
	m_textColLocation = glGetUniformLocation(main_program, "textCol");
}

void FontShader::setTextureLocation(unsigned int loc)
{
	glUniform1i(m_textureLocation, loc);
}

void FontShader::setColour(glm::vec3 colour) {
	glUniform3f(m_textColLocation, colour.x, colour.y, colour.z);
}
