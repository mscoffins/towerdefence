#pragma once

#include "AStarNode.h"
#include <glm/glm.hpp>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include <iostream>

class AStar{
private: 
	std::vector<AStarNode*> openList;
	std::vector<AStarNode*> closedList;
	AStarNode* startNode;
	AStarNode* goalNode;
	std::vector<AStarNode*> mapNodes;
	int* map;
	int gridSize;

public:
	AStar(int* map, bool d, int gridSize);
	int huristic(AStarNode* pos);
	std::vector<glm::vec2> search(glm::vec2 start, glm::vec2 end);
	AStarNode* getbestNode();
	std::vector<glm::vec2> getPath(AStarNode* end);
	void linkChild(AStarNode* parent, AStarNode temp, int weight);
	void updateParents(AStarNode* node);
	AStarNode* checkList(std::vector<AStarNode*> list, int id);
	int getBestNodePos(int id);
	void draw();
	bool debug;
	double timeCheckList;
	double updateParentsTime;
	int count;
	~AStar() {
		while (!mapNodes.empty()) {
			AStarNode* n = mapNodes.back();
			mapNodes.pop_back();
			delete n;
		}
	}
};