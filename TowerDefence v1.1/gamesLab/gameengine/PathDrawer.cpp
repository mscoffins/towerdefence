#include "PathDrawer.h"

PathDrawer::PathDrawer(AssetLib * assets)
{
	path = vector<glm::vec2>();
	pathLine = new ParticleSystem(ParticleSystem::PathParticles, ParticleSystem::PathEmmiter, glm::vec3(5, 7, -80), assets->particleShader, assets->particleTextures[5]);
	shadowShader = assets->particleShadowShader;
}

void PathDrawer::setPath(vector<glm::vec2> path)
{
	pathLine->setSystemCenter(glm::vec3(5, 7, -80));
	float gridSize = GRIDSIZE;
	this->path = vector<glm::vec2>();
	for (int i = 80; i > 45; i--)
	{
		this->path.push_back(glm::vec2(-5, i));
	}
	
	glm::vec2 p0, p1, p2, p3;
	for (int i = 0; i < path.size()-1; i++) {
		if (i == 0)
			p0 = glm::vec2(-5, 55);
		else
			p0 = path[i - 1] * gridSize - (gridSize*(gridSize / 2)) + (gridSize / 2);

		p1 = path[i] * gridSize - (gridSize*(gridSize / 2)) + (gridSize / 2);
		p2 = path[i+1] * gridSize - (gridSize*(gridSize / 2)) + (gridSize / 2);

		if (i == path.size() - 2)
			p3 = glm::vec2(5, -55);
		else
			p3 = path[i + 2] * gridSize - (gridSize*(gridSize / 2)) + (gridSize / 2);

		CatmullRomSpline x = CatmullRomSpline(p0.x, p1.x, p2.x, p3.x);
		CatmullRomSpline y = CatmullRomSpline(p0.y, p1.y, p2.y, p3.y);

		for (int j = 0; j < 10; j++) {
			float t = j * 0.1;	
			this->path.push_back(glm::vec2(x.PointAlongLine(t),y.PointAlongLine(t)));
		}
	}
	for (int i = -45; i >= -80; i--)
	{
		this->path.push_back(glm::vec2(5, i));
	}
	//this->path.push_back(glm::vec2(5, -80));
	navigatingPath = this->path;
}

void PathDrawer::Init()
{
	pathLine->init();
}

void PathDrawer::Draw(Camera & cam)
{
	pathLine->draw(cam);
}

void PathDrawer::Update(double deltaT, Camera & cam)
{
	glm::vec3 worldPos = pathLine->getSystemCenter();
	float speed = path.size()/5 + 7.5;
	if (navigatingPath.size() > 0) {

		glm::vec3 targetPos = glm::vec3(navigatingPath.back().x, 7, navigatingPath.back().y);

		if (glm::length(targetPos - worldPos) > speed*deltaT) {
			glm::vec3 moveVec = glm::normalize(targetPos - worldPos);
			worldPos += moveVec * (float)(speed * deltaT);
		}
		else {
			worldPos = targetPos;
			navigatingPath.pop_back();
		}
	}
	else {
		navigatingPath = path;
		worldPos = glm::vec3(5, 7, -80);
	}
	pathLine->setSystemCenter(worldPos);
	pathLine->update(deltaT, cam);
}

void PathDrawer::ShadowMapPass(glm::mat4 depthMVP)
{
	shadowShader->Bind();
	shadowShader->SetWVP(depthMVP);
	pathLine->shadowMapPass(shadowShader);
}
