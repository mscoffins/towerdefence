#pragma once
#include "Shader\Shader.h"

class ShadowMapShader : public Shader {

public:

	ShadowMapShader() {}

	ShadowMapShader(const std::string& filename);

	void SetWVP(const glm::mat4 WVP);

private:

	GLuint m_MVPLocation;
};