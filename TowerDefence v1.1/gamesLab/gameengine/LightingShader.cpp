#include "LightingShader.h"


LightingShader::LightingShader(const std::string& filename):Shader(filename)
{
	m_WVPLocation = glGetUniformLocation(main_program,"gWVP");
	m_WorldMatrixLocation = glGetUniformLocation(main_program, "gWorld");
	m_samplerLocation = glGetUniformLocation(main_program, "gSampler");
	m_eyeWorldPosLocation = glGetUniformLocation(main_program, "gEyeWorldPos");
	m_dirLightLocation.Color = glGetUniformLocation(main_program, "gDirectionalLight.Base.Color");
	m_dirLightLocation.AmbientIntensity = glGetUniformLocation(main_program, "gDirectionalLight.Base.AmbientIntensity");
	m_dirLightLocation.Direction = glGetUniformLocation(main_program, "gDirectionalLight.Direction");
	m_dirLightLocation.DiffuseIntensity = glGetUniformLocation(main_program, "gDirectionalLight.Base.DiffuseIntensity");
	m_matSpecularIntensityLocation = glGetUniformLocation(main_program, "gMatSpecularIntensity");
	m_matSpecularPowerLocation = glGetUniformLocation(main_program, "gSpecularPower");
	m_lightWVPLocation = glGetUniformLocation(main_program, "gLightWVP");
	m_shadowMapLocation = glGetUniformLocation(main_program, "shadowMap");

	m_numPointLightsLocation = glGetUniformLocation(main_program, "numPointLights");

	for (int i = 0; i < MAX_POINT_LIGHTS; i++)
	{
		string name = "gPointLights[" + to_string(i) + "].Base.Color";
		m_pointLightsLocation[i].Color = glGetUniformLocation(main_program,name.c_str());
		name = "gPointLights[" + to_string(i) + "].Base.AmbientIntensity";
		m_pointLightsLocation[i].AmbientIntensity = glGetUniformLocation(main_program, name.c_str());
		name = "gPointLights[" + to_string(i) + "].Base.DiffuseIntensity";
		m_pointLightsLocation[i].DiffuseIntensity = glGetUniformLocation(main_program, name.c_str());
		name = "gPointLights[" + to_string(i) + "].Position";
		m_pointLightsLocation[i].Position = glGetUniformLocation(main_program, name.c_str());
		name = "gPointLights[" + to_string(i) + "].Atten.Constant";
		m_pointLightsLocation[i].Atten.Constant = glGetUniformLocation(main_program, name.c_str());
		name = "gPointLights[" + to_string(i) + "].Atten.Linear";
		m_pointLightsLocation[i].Atten.Linear = glGetUniformLocation(main_program, name.c_str());
		name = "gPointLights[" + to_string(i) + "].Atten.Exp";
		m_pointLightsLocation[i].Atten.Exp = glGetUniformLocation(main_program, name.c_str());
	}
}

void LightingShader::SetWVP(glm::mat4 WVP)
{
	glUniformMatrix4fv(m_WVPLocation, 1, GL_FALSE, &WVP[0][0]);
}


void LightingShader::SetWorldMatrix(glm::mat4 WorldInverse)
{
	glUniformMatrix4fv(m_WorldMatrixLocation, 1, GL_FALSE, &WorldInverse[0][0]);
}


void LightingShader::SetTextureUnit(unsigned int TextureUnit)
{
	glUniform1i(m_samplerLocation, TextureUnit);
}


void LightingShader::SetDirectionalLight(const DirectionalLight& Light)
{
	glUniform3f(m_dirLightLocation.Color, Light.Color.x, Light.Color.y, Light.Color.z);
	glUniform1f(m_dirLightLocation.AmbientIntensity, Light.AmbientIntensity);
	glm::vec3 Direction = Light.Direction;
	glm::normalize(Direction);
	glUniform3f(m_dirLightLocation.Direction, Direction.x, Direction.y, Direction.z);
	glUniform1f(m_dirLightLocation.DiffuseIntensity, Light.DiffuseIntensity);

}

void LightingShader::SetEyeWorldPos(const glm::vec3& EyeWorldPos)
{
	glUniform3f(m_eyeWorldPosLocation, EyeWorldPos.x, EyeWorldPos.y, EyeWorldPos.z);
}

void LightingShader::SetMatSpecularIntensity(float Intensity)
{
	glUniform1f(m_matSpecularIntensityLocation, Intensity);
}

void LightingShader::SetMatSpecularPower(float Power)
{
	glUniform1f(m_matSpecularPowerLocation, Power);
}

void LightingShader::SetLightWVP(glm::mat4 lWVP)
{
	glUniformMatrix4fv(m_lightWVPLocation, 1, GL_FALSE, &lWVP[0][0]);
}

void LightingShader::SetShadowMap(unsigned int ShadowMapUint)
{
	glUniform1i(m_shadowMapLocation, ShadowMapUint);
}

void LightingShader::SetPointLights(vector<PointLight> pLights)
{
	int size = pLights.size();
	if (size > MAX_POINT_LIGHTS)
		size = MAX_POINT_LIGHTS;
	glUniform1i(m_numPointLightsLocation, size);

	for (int i = 0; i < size; i++) {
		glUniform3f(m_pointLightsLocation[i].Color, pLights[i].Color.x, pLights[i].Color.y, pLights[i].Color.z);
		glUniform1f(m_pointLightsLocation[i].AmbientIntensity, pLights[i].AmbientIntensity);
		glUniform1f(m_pointLightsLocation[i].DiffuseIntensity, pLights[i].DiffuseIntensity);
		glUniform3f(m_pointLightsLocation[i].Position, pLights[i].Position.x, pLights[i].Position.y, pLights[i].Position.z);
		glUniform1f(m_pointLightsLocation[i].Atten.Constant, pLights[i].Attenuation.Constant);
		glUniform1f(m_pointLightsLocation[i].Atten.Linear, pLights[i].Attenuation.Linear);
		glUniform1f(m_pointLightsLocation[i].Atten.Exp, pLights[i].Attenuation.Exp);
	}
}
