#include "CatmullRomSpline.h"

CatmullRomSpline::CatmullRomSpline(float p0, float p1, float p2, float p3)
{
	this->p0 = p0;
	this->p1 = p1;
	this->p2 = p2;
	this->p3 = p3;
}

float CatmullRomSpline::PointAlongLine(float t)
{
	return 0.5f * ((2 * p1) +
					(p2 - p0) * t +
					(2 * p0 - 5 * p1 + 4 * p2 - p3) * t * t +
					(3 * p1 - p0 - 3 * p2 + p3) * t * t * t);

}
