#include "GuiShader.h"

GuiShader::GuiShader(const std::string & filename):Shader(filename)
{
	m_textureLocation = glGetUniformLocation(main_program, "diffuse");
	isSelectedLocation = glGetUniformLocation(main_program, "is_selected");
}

void GuiShader::setTextureLocation(unsigned int loc)
{
	glUniform1i(m_textureLocation, loc);
}

void GuiShader::setIsSelected(int selected)
{
	glUniform1i(isSelectedLocation, selected);
}
