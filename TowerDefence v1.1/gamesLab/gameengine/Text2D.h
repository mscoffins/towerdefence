#pragma once

#include <vector>
#include <cstring>

#include<gl\glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "FontShader.h"
#include "ObjectLoading\Texture.h"

class Text2D {
private:
	GLuint Text2DVertexBufferID;
	GLuint Text2DUVBufferID;
	GLuint VAO;
	Texture* tex;
	FontShader* shader;
	float charWidth;
public:
	Text2D() {}
	Text2D(const std::string& filename, float charWidth );
	void init();
	void printString(const char * text, int x, int y, int size, glm::vec3 colour);

};