#pragma once
#include "Shader\Shader.h"

class FontShader : public Shader {

public:

	FontShader() {}

	FontShader(const std::string& filename);

	void setTextureLocation(unsigned int loc);
	void setColour(glm::vec3 colour);
private:
	GLuint m_textureLocation;
	GLuint m_textColLocation;
};