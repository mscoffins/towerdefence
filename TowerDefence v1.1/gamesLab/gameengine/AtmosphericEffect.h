#pragma once
#include "ParticleSystem.h"
#include "AssetLib.h"

enum EffectType {TOWER_BUILD_SMOKE, PORTAL_EMMISION, BUTTERFLYS, RUBBLE, SELECTED_TOWER, CONFETTI_EXPLOSION};

class AtmosphericEffect {
private:
	ParticleSystem* effect;
	ShadowMapParticleShader* shadowShader;
	float duration;
	float deathTimer;
	

public:
	AtmosphericEffect(){}
	AtmosphericEffect(AssetLib* assets, glm::vec3 worldPos, EffectType type);

	void init();
	void draw(Camera& cam);
	void update(Camera& cam, double deltaT);
	void shadowMapPass(glm::mat4 depthMVP);
	void setDuration(float dur) { duration = dur; }
	void resetDeathTimer() { deathTimer = effect->getMaxParticleLife(); }
	void setWorldPos(glm::vec3 pos) { effect->setSystemCenter(pos); }
	bool finished;
	glm::vec3 getWorldPos() { return effect->getSystemCenter(); }
	~AtmosphericEffect() {
		delete effect;
	}
};