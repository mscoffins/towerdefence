#pragma once
#include "GUI\Button.h"

class Menu{
private:
	GuiShader* shader;

	bool showControls;
	

	Button* playBtn;
	Button* controlsBtn;
	Button* backBtn;

	Vert* verts;

	Mesh* background;
	Mesh* controls;
	Texture* textureBG;
	Texture* textureControls;

public:
	Menu() {}
	Menu(GuiShader* shader);

	void Init();
	
	void buttonCollision(glm::vec4 normalisedCoords);

	void Draw(glm::vec4 normalisedCoords);
	bool playGame;
};