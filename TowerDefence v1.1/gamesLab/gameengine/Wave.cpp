#include "Wave.h"

Wave::Wave(vector<EnemyType> enemys, AssetLib* assets, vector<glm::vec2> path, float statMod)
{
	for each (EnemyType t in enemys)
	{
		this->enemys.push_back(new Enemy(t, assets, path, statMod));
	}
	for each (Enemy* e in this->enemys)
	{
		e->Init();
	}
	spawnRate = 5;
	timeSinceSpawn = 0;
	enemiesFinished = 0;
	enemiesKilled = 0;
	waveFinished = false;
	waveEarnings = 0;
}

void Wave::Update(double deltaT, Camera & cam)
{
	timeSinceSpawn -= deltaT;
	if (timeSinceSpawn <= 0) {
		if (enemys.size() > 0) {
			activeEnemies.push_back(enemys.back());
			enemys.pop_back();
		}
		timeSinceSpawn = spawnRate;
	}

	for each (Enemy* e in activeEnemies){
		if (e != nullptr) {
			e->Update(deltaT, cam);
		}
	}


	for (int i = 0; i < activeEnemies.size(); i++)
	{
		if (activeEnemies[i] != nullptr) {
			if (activeEnemies[i]->deathTimer <= 0) {
				if (activeEnemies[i]->finished)
					enemiesFinished++;
				else {
					enemiesKilled++;
					waveEarnings += activeEnemies[i]->getValue();
				}
				delete activeEnemies[i];
				activeEnemies[i] = nullptr;
			}
		}

	}
	auto removeItt = remove_if(activeEnemies.begin(), activeEnemies.end(), [](Enemy* e) {if (e == nullptr) return true; return false; });

	activeEnemies.erase(removeItt, activeEnemies.end());

	if (enemys.size() == 0 && activeEnemies.size() == 0) {
		//cout << "wave finished" << endl;
		waveFinished = true;
	}

}

void Wave::Draw(Camera & cam)
{
	for each ( Enemy* e in activeEnemies)
	{
		if (e != nullptr)
			e->Draw(cam);
	}
}

void Wave::shadowMapPass(glm::mat4 depthMVP)
{
	for each (Enemy* e in activeEnemies)
	{
		if (e != nullptr)
			e->ShadowPass(depthMVP);
	}
}
