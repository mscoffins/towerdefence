#pragma once

#include "ObjectLoading\Assimp\AssimpModel.h"
#include "ObjectLoading\Texture.h"
#include "ParticleShader.h"
#include "LightingShader.h"
#include "FontShader.h"
#include "ShadowMapShader.h"
#include "ShadowMapParticleShader.h"
#include "GuiShader.h"
#include "Text2D.h"
#include "../include/SDL2/SDL_mixer.h"


class AssetLib {
public:
	vector<AssimpModel*> towers;
	AssimpModel* world;
	AssimpModel* grid;
	AssimpModel* background;
	ParticleShader* particleShader;
	LightingShader* lightingShader;
	FontShader* fontShader;
	ShadowMapShader* shadowShader;
	ShadowMapParticleShader* particleShadowShader;
	GuiShader* guiShader;
	Text2D* font;
	vector<pair<Texture*, glm::vec3>> particleTextures;

	Mix_Music* bgMusic = NULL;

	Mix_Chunk* turretFire = NULL;
	Mix_Chunk* freezeFire = NULL;
	Mix_Chunk* mortarFire = NULL;
	Mix_Chunk* buildTower = NULL;
	Mix_Chunk* destroyTower = NULL;
	Mix_Chunk* upgradeTower = NULL;
	AssetLib() {}

	void LoadAssets();

};
