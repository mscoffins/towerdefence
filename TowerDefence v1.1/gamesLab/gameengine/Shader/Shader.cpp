#include "Shader.h"
#include <fstream>
#include <iostream>


Shader::Shader(const string& fileName)
{
	main_program = glCreateProgram(); // Create new program
	main_shaders[0] = CreateShader(LoadShader(fileName + ".vert"), GL_VERTEX_SHADER); // Create a new vertex shader
	main_shaders[1] = CreateShader(LoadShader(fileName + ".frag"), GL_FRAGMENT_SHADER); // Create a new fragment shader

	for (int i = 0;i < NUM_SHADERS;i++)
		glAttachShader(main_program, main_shaders[i]); // Attach shaders to our program

	bool failed = false;

	glLinkProgram(main_program); // Link the program
	failed = CheckShaderError(main_program, GL_LINK_STATUS, true, "Error: Shader failed to link with program."); // Check for shader link errors

	glValidateProgram(main_program); // Validate the program
	failed = CheckShaderError(main_program, GL_VALIDATE_STATUS, true, "Error: Validation of program failed."); // Check for shader validation errors

	if (!failed)
		cout << "Shader: "<< fileName << " Loaded Succesfully" << endl;

	openGLErrorCheck(__FILE__, __LINE__);
}

Shader::~Shader()
{
	for (int i = 0;i < NUM_SHADERS;i++) // Unbind and delete shaders
	{
		glDetachShader(main_program, main_shaders[i]);
		glDeleteShader(main_shaders[i]);
	}
	glDeleteProgram(main_program); // Remove program
}

void Shader::Bind()
{
	glUseProgram(main_program); // Bind the shader to the program
	openGLErrorCheck(__FILE__, __LINE__);
}


GLuint Shader::CreateShader(const string& input, GLenum type)
{
	GLuint shader = glCreateShader(type);
	openGLErrorCheck(__FILE__, __LINE__);

	if (shader == 0)
		cerr << "Error: Failed to create shader." << endl;

	const GLchar* shaderStringList[1];
	GLint shaderStringListLength[1];

	shaderStringList[0] = input.c_str();
	shaderStringListLength[0] = input.length();

	glShaderSource(shader, 1, shaderStringList, shaderStringListLength);
	glCompileShader(shader); // Build shader

	CheckShaderError(shader, GL_COMPILE_STATUS, false, "Error: Shader failed to compile."); // Check for shader compilation errors

	return shader;
}

string Shader::LoadShader(const string& fileName)
{
	ifstream file;
	file.open((fileName).c_str());

	string output, line;
	
	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
		cerr << "Unable to load shader: " << fileName << endl;
	return output;
}

bool Shader::CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const string& errorMessage)
{
	GLint success = 0;
	GLchar error[1024] = { 0 };

	if (isProgram)
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);

	if (success == GL_FALSE)
	{
		if (isProgram)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		cerr << errorMessage << ": '" << error << "'" << endl;

		return true;

	}
	return false;
}





