#version 330                                                                        
                                                                                    
layout (location = 0) in vec4 Position;                                             
layout (location = 1) in vec4 color;                                             
layout (location = 2) in float life;                                                                                                                              
uniform mat4 gLightWVP;                             

out float lifeRemaining;
                                                                                    
void main()                                                                         
{                                                                                   
    gl_Position = gLightWVP * vec4(Position.xyz, 1.0);
    gl_PointSize = Position.w * 5;
	lifeRemaining = life;                           
}

