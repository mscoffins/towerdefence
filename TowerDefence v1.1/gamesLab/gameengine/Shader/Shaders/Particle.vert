#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec4 xyzs; // Position of the center of the particule and size of the square
layout(location = 1) in vec4 color; // Position of the center of the particule and size of the square
layout(location = 2) in float life;
// Output data ; will be interpolated for each fragment.
out vec4 particlecolor;
out float lifeRemaining;
// Values that stay constant for the whole mesh.

uniform mat4 VP; // Model-View-Projection matrix, but without the Model (the position is in BillboardPos; the orientation depends on the camera)
uniform float nearPlaneHeight;

void main()
{
	vec3 particleCenter_wordspace = xyzs.xyz;
	
	vec3 vertexPosition_worldspace = particleCenter_wordspace;

	// Output position of the vertex
	gl_Position = VP * vec4(vertexPosition_worldspace, 1.0f);
	gl_PointSize = (nearPlaneHeight * xyzs.w) / gl_Position.w;

	particlecolor = color;
	lifeRemaining = life;
}

