#version 330                                                                        
                                                                                    
in vec2 TexCoord0;                                                                  
in vec3 Normal0;                                                                    
in vec3 WorldPos0;                                                                  
in vec4 LightPos0;                                                                                    
out vec4 FragColor;                                                                 

const int MAX_POINT_LIGHTS = 120;

struct BaseLight{
	vec3 Color;                                                                     
    float AmbientIntensity;                                                         
    float DiffuseIntensity;  
};
                                                                                    
struct DirectionalLight                                                             
{                                                                                   
    BaseLight Base;                                                        
    vec3 Direction;                                                                 
};                                                                                  

struct Attenuation                                                                  
{                                                                                   
    float Constant;                                                                 
    float Linear;                                                                   
    float Exp;                                                                      
}; 

struct PointLight
{
	vec3 Position;
	BaseLight Base;
	Attenuation Atten;
};

uniform PointLight gPointLights[MAX_POINT_LIGHTS];                                                                                 
uniform DirectionalLight gDirectionalLight;                                         
uniform sampler2D gSampler; 
uniform sampler2D shadowMap;                                                        
uniform vec3 gEyeWorldPos;                                                          
uniform float gMatSpecularIntensity;                                                
uniform float gSpecularPower;

uniform int numPointLights;

vec2 poissonDisk[16] = vec2[]( 
   vec2( -0.94201624, -0.39906216 ), 
   vec2( 0.94558609, -0.76890725 ), 
   vec2( -0.094184101, -0.92938870 ), 
   vec2( 0.34495938, 0.29387760 ), 
   vec2( -0.91588581, 0.45771432 ), 
   vec2( -0.81544232, -0.87912464 ), 
   vec2( -0.38277543, 0.27676845 ), 
   vec2( 0.97484398, 0.75648379 ), 
   vec2( 0.44323325, -0.97511554 ), 
   vec2( 0.53742981, -0.47373420 ), 
   vec2( -0.26496911, -0.41893023 ), 
   vec2( 0.79197514, 0.19090188 ), 
   vec2( -0.24188840, 0.99706507 ), 
   vec2( -0.81409955, 0.91437590 ), 
   vec2( 0.19984126, 0.78641367 ), 
   vec2( 0.14383161, -0.14100790 ) 
);                                                        

vec4 CalcLightInternal(BaseLight Light, vec3 LightDirection, vec3 Normal)                   
{                                                                                           
    //vec4 AmbientColor = vec4(Light.Color * Light.AmbientIntensity, 1.0f);
    float DiffuseFactor = dot(Normal, -LightDirection);                                     
                                                                                            
    vec4 DiffuseColor  = vec4(0, 0, 0, 0);                                                  
    vec4 SpecularColor = vec4(0, 0, 0, 0);                                                  
                                                                                            
    if (DiffuseFactor > 0) {                                                                
        DiffuseColor = vec4(Light.Color * Light.DiffuseIntensity * DiffuseFactor, 1.0f);    
                                                                                            
        vec3 VertexToEye = normalize(gEyeWorldPos - WorldPos0);                             
        vec3 LightReflect = normalize(reflect(LightDirection, Normal));                     
        float SpecularFactor = dot(VertexToEye, LightReflect);                                      
        if (SpecularFactor > 0) {                                                           
            SpecularFactor = pow(SpecularFactor, gSpecularPower);
            SpecularColor = vec4(Light.Color * gMatSpecularIntensity * SpecularFactor, 1.0f);
        }                                                                                   
    }                                                                                       
                                                                                            
    return (DiffuseColor + SpecularColor);                                   
}                                                                                           
                                                                                            
vec4 CalcDirectionalLight(vec3 Normal)                                                      
{                                                                                           
    return CalcLightInternal(gDirectionalLight.Base, gDirectionalLight.Direction, Normal); 
}                                                                                           
                                                                                            
vec4 CalcPointLight(int Index, vec3 Normal)                                                 
{                                                                                           
    vec3 LightDirection = WorldPos0 - gPointLights[Index].Position;                         
    float Distance = length(LightDirection);                                                
    LightDirection = normalize(LightDirection);                                             
                                                                                            
    vec4 Color = CalcLightInternal(gPointLights[Index].Base, LightDirection, Normal);       
    float Attenuation =  gPointLights[Index].Atten.Constant +                               
                         gPointLights[Index].Atten.Linear * Distance +                      
                         gPointLights[Index].Atten.Exp * Distance * Distance;               
                                                                                            
    return Color / Attenuation;                                                             
}                                                                                       
 
vec4 CalcPointLightAmbient(int Index)                                                 
{                                                                                           
    vec3 LightDirection = WorldPos0 - gPointLights[Index].Position;                         
    float Distance = length(LightDirection);                                                
    LightDirection = normalize(LightDirection);                                             
                                                                                            
    vec4 Color = vec4(gPointLights[Index].Base.Color * gPointLights[Index].Base.AmbientIntensity, 1.0f);      
    float Attenuation =  gPointLights[Index].Atten.Constant +                               
                         gPointLights[Index].Atten.Linear * Distance +                      
                         gPointLights[Index].Atten.Exp * Distance * Distance;               
                                                                                            
    return Color / Attenuation;                                                             
} 
                                                                                    
void main()                                                                         
{                                                                                   
    vec4 AmbientColor = vec4(gDirectionalLight.Base.Color * gDirectionalLight.Base.AmbientIntensity, 1.0f);
    vec3 LightDirection = -gDirectionalLight.Direction;                             
    vec3 Normal = normalize(Normal0);                                               
                                                                                    
    float DiffuseFactor = dot(Normal, LightDirection);                              
                                                                                    
    //vec4 DiffuseColor  = vec4(0, 0, 0, 0);                                          
    //vec4 SpecularColor = vec4(0, 0, 0, 0);                                          
                                                                                    
    //if (DiffuseFactor > 0) {                                                        
    //    DiffuseColor = vec4(gDirectionalLight.Base.Color * gDirectionalLight.Base.DiffuseIntensity * DiffuseFactor, 1.0f);
                                                                                    
    //    vec3 VertexToEye = normalize(gEyeWorldPos - WorldPos0);                     
    //    vec3 LightReflect = normalize(reflect(gDirectionalLight.Direction, Normal));
    //    float SpecularFactor = dot(VertexToEye, LightReflect);                      
    //    if (SpecularFactor > 0) {                                                   
    //        SpecularFactor = pow(SpecularFactor, gSpecularPower);
    //        SpecularColor = vec4(gDirectionalLight.Base.Color * gMatSpecularIntensity * SpecularFactor, 1.0f);
    //    }                                                                           
    //}

	vec4 TotalLight = CalcDirectionalLight(Normal);
	vec4 pointLightSum =  vec4(0,0,0,0);
	for (int i = 0 ; i < numPointLights; i++) {                                           
        pointLightSum += CalcPointLight(i, Normal);
		AmbientColor +=  CalcPointLightAmbient(i);                                    
    } 

    float visibility = 1.0;
	vec3 ProjCoords = LightPos0.xyz / LightPos0.w;
	vec2 UVCoords;
	UVCoords.x = 0.5 * ProjCoords.x + 0.5;
	UVCoords.y = 0.5 * ProjCoords.y + 0.5;
	float z = 0.5 * ProjCoords.z + 0.5;

	float bias = 0.005*tan(acos(DiffuseFactor));
	bias = clamp(bias, 0,0.01);

	float centerDepth = texture(shadowMap,UVCoords.xy).r;

	for (int i=0;i<16;i++){
		
		int index = i;
		// being fully in the shadow will eat up 16*0.05 = 0.8
		// 0.2 potentially remain, which is quite dark.
		float Depth = texture(shadowMap,UVCoords.xy + poissonDisk[i]/1000.0).r;
		if(Depth < (z - bias))
			visibility -= 0.05;
	}
	
	if(UVCoords.x > 1 || UVCoords.x < 0 || UVCoords.y > 1 || UVCoords.y < 0 || z < 0 || z > 1)
		visibility = 1.0;
	                                                                            
    //FragColor = texture2D(gSampler, TexCoord0.xy) * (AmbientColor + visibility *( DiffuseColor + SpecularColor));
	FragColor = texture2D(gSampler, TexCoord0.xy) * (AmbientColor + pointLightSum + visibility * TotalLight);
                    
}

