#version 330 

out vec3 FragColor; 

in float lifeRemaining;

uniform sampler2D myTextureSampler;
uniform float maxLife;
uniform vec3 ssDims;
void main(){

	vec2 cellLoc;
	cellLoc.x = gl_PointCoord.x / ssDims.x;
	cellLoc.y = gl_PointCoord.y / ssDims.y;
	float lifeRatio = (maxLife - lifeRemaining)/maxLife;
	int cellCount = int(lifeRatio*ssDims.z);
	int cellPosX = cellCount%int(ssDims.x);
	int cellPosY = cellCount/int(ssDims.x);

	vec2 cellOrigin = vec2(cellLoc.x + cellPosX/ssDims.x, cellLoc.y + cellPosY/ssDims.y); 
	vec4 temp = texture( myTextureSampler, cellOrigin );
	
	if(temp.a < 0.7)
		discard;
	
	gl_FragDepth = gl_FragCoord.z;
    FragColor = vec3(gl_FragCoord.z);
}