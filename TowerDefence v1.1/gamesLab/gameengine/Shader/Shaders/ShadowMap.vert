#version 330                                                                        
                                                                                    
layout (location = 0) in vec3 Position;                                             
layout (location = 1) in vec2 TexCoord;                                             
layout (location = 2) in vec3 Normal;                                               

out vec2 texCoord0;                                                                                                                                 
uniform mat4 gLightWVP;                                                                                                                                                   
                                                                                    
void main()                                                                         
{                                                                                   
    gl_Position = gLightWVP * vec4(Position, 1.0);
    texCoord0 = TexCoord;                             
}

