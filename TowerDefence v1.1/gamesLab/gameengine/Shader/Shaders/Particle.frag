#version 330 core

// Interpolated values from the vertex shaders
//in vec2 UV;
in vec4 particlecolor;
in float lifeRemaining;
// Ouput data
out vec4 color;

uniform sampler2D myTextureSampler;
uniform float maxLife;
uniform vec3 ssDims;
uniform float alphaFactor;

void main(){
	vec2 cellLoc;
	cellLoc.x = gl_PointCoord.x / ssDims.x;
	cellLoc.y = gl_PointCoord.y / ssDims.y;
	float lifeRatio = (maxLife - lifeRemaining)/maxLife;
	int cellCount = int(lifeRatio*ssDims.z);
	int cellPosX = cellCount%int(ssDims.x);
	int cellPosY = cellCount/int(ssDims.x);
	//cellPosX = 1;
	//cellPosY = 6;
	vec2 cellOrigin = vec2(cellLoc.x + cellPosX/ssDims.x, cellLoc.y + cellPosY/ssDims.y); 
	// Output color = color of the texture at the specified UV
	vec4 temp = texture( myTextureSampler, cellOrigin );
	//vec4 temp = texture( myTextureSampler, gl_PointCoord );
	temp.w = temp.w/alphaFactor;
	color =  temp;
	//color = vec4(1,1,1,cellPosY/ssDims.y);
	//color = particlecolor;
}