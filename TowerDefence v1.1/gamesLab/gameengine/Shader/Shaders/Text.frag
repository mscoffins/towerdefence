#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 colour;
// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D gSampler;
uniform vec3 textCol;

void main()
{

	color = vec4(textCol.x, textCol.y, textCol.z,texture( gSampler, UV ).a);
	//color = vec4(1, 1, 1,texture( gSampler, UV ).a);
	
}