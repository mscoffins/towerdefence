#version 430

uniform sampler2D diffuse;

uniform int is_selected;

in vec3 vert_normal;
in vec2 vert_col;

out vec4 out_col;

void main()
{
	vec4 texCol = texture(diffuse, vert_col);
	if(is_selected == 2){	
		out_col = vec4(mix(texCol, vec4(1,1,0.2,texCol.a), 0.6f));
	}
	else{ 
		if(is_selected == 1){
			out_col = vec4(mix(texCol, vec4(0.2,0.6,0.7,texCol.a), 0.6f));
		}
		else{
			out_col = texCol;
		}
	}
}