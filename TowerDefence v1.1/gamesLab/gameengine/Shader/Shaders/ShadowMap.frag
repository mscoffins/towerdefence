#version 330 
in vec2 texCoord0;
uniform sampler2D gSampler;
out vec3 FragColor; 

void main(){
	vec4 col = texture2D(gSampler, texCoord0.xy);
	if(col.a < 0.2)
		discard;
	gl_FragDepth = gl_FragCoord.z;
    FragColor = vec3(gl_FragCoord.z);
}