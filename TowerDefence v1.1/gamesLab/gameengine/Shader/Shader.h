#pragma once
using namespace std;
#include<string>
#include<gl\glew.h>
#include "../GameObjects/Transform.h"
#include "../GameObjects/Camera.h"
#include "../OpenGLHelper.h"

class Shader
{
public:
	Shader() {}
	Shader(const string& fileName);

	void Bind();

	virtual ~Shader();

private:
	static const unsigned int NUM_SHADERS = 2;
	Shader(const Shader& o) {}
	void operator*(const Shader& o) {}

	static bool CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const string& errorMessage);
	static string LoadShader(const string& fileName);
	static GLuint CreateShader(const string& input, GLenum type);

protected:
	GLuint main_program;
	GLuint main_shaders[NUM_SHADERS];
};

