#pragma once
#include "Shader\Shader.h"

class GuiShader : public Shader {

public:

	GuiShader() {}

	GuiShader(const std::string& filename);

	void setTextureLocation(unsigned int loc);

	void setIsSelected(int selected);

private:
	GLuint m_textureLocation;
	GLuint isSelectedLocation;
};