#pragma once

#include "glm\glm.hpp"
#include <math.h>

class CatmullRomSpline {
private:
	float p0, p1, p2, p3;
public:
	CatmullRomSpline(float p0 = 0, float p1 = 0, float p2 = 0, float p3 = 0);
	float PointAlongLine(float t);
};