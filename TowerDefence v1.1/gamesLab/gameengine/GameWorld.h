#pragma once
#include "Tower.h"
#include "Wave.h"
#include "AtmosphericEffect.h"
#include "PathDrawer.h"
#include "AStar\AStar.h"
#include "AssetLib.h"
#include "GUI\GUI.h"
#include <queue>
#include "ShadowMapFBO.h"
#include "DefinedVars.h"
#include <thread>

class GameWorld {
private:
	AssetLib* assets;
	AssimpModel* worldModel;
	AssimpModel* grid;
	AssimpModel* BuildTower;
	int map[GRIDSIZE*GRIDSIZE];
	vector<Tower*> towers;
	vector<AtmosphericEffect*> effects;
	vector<PointLight> pLights;
	queue<Wave*> waves;
	Wave* activeWave;
	int resources;
	vector<glm::vec2> path;
	PathDrawer* pathDrawer;

	AtmosphericEffect* selectedTowerEffect;

	Text2D* text;
	
	bool waveFinished;

	int waveCount = 1;
	int enemiesKilled = 0;
	int enemiesFinished = 0;
	int lives = 10;

	

public:
	GameWorld() {}
	GameWorld(AssetLib* assets);
	void Draw(Camera& cam, glm::vec4 normalisedCoords, glm::mat4 depthMVP, float mouseX, float mouseY, ShadowMapFBO* smFBO, DirectionalLight dirlight);
	void ShadowMapPass(glm::mat4 depthMVP);
	void Update(double deltaT, Camera& cam);
	void Init();

	void addTower();
	void selectTower(float mouseX, float mouseY, Camera& cam);
	void resetWorld();


	GUI* gui;
	glm::vec3 buildWorldPos;
	glm::vec2 buildCellPos;
	float waveDelay;
	bool gameOver = false;
};