#pragma once
#include "../GameObjects/Primitives/Mesh.h"
#include "../ObjectLoading/Texture.h"
#include <vector>
#include "../gameobjects/Primitives/Ray.h"
#include "Button.h"
#include "../GuiShader.h"
#include "../Tower.h"

#define GUIBORDER_X 0.05
#define GUIBORDER_Y 0.035

#define MULTIBUTTONSIZE_X 0.35
#define MULTIBUTTONSIZE_Y 0.25

#define SINGLEBUTTONSIZE_X 0.2
#define SINGLEBUTTONSIZE_Y 0.07


class GUI
{
public:
	GUI(GuiShader* guiShader);
	GUI(Mesh &m) { this->background = &m; };
	~GUI();

	void Init();
	void Draw(glm::vec4 normalisedCoords);

	void checkButtonCollision(glm::vec4 normalisedCoords);
	bool checkGUIcollision(glm::vec4 normalisedCoords);

	void resetGUI();
	//Check Button Methods
	Vert* verts;
	GuiShader* GUIShader;

	Mesh* background;
	Texture* textureGUI;

	//Buttons
	std::vector < pair<Button*, int>> TowerButtons;

	Button* sellButton;
	Button* upgradeButton;

	int selectedTower;
	Tower* targetTower;
	bool activeTowerSelected;
};