#include "GUI.h"

GUI::GUI(GuiShader* guiShader)
{ 
	this->verts = new Vert [4] {
		Vert(glm::vec3(-1.0,-1.0,0), glm::vec2(0.0,1.0), glm::vec3(0.0,0.0,-1.0)),
		Vert(glm::vec3(-0.5,-1.0,0), glm::vec2(1.0,1.0), glm::vec3(0.0,0.0,-1.0)),
		Vert(glm::vec3(-0.5,1.0,0), glm::vec2(1.0,0.0), glm::vec3(0.0,0.0,-1.0)),
		Vert(glm::vec3(-1.0,1.0,0), glm::vec2(0.0,0.0), glm::vec3(0.0,0.0,-1.0))

	};

	unsigned int indices[] = { 0,1,2,0,2,3 };
	selectedTower = -1;
	this->background = new Mesh(verts, sizeof(verts), indices, sizeof(indices));
	this->GUIShader = guiShader;
}

GUI::~GUI()
{
}

void GUI::Init()
{

	//texture for GUI background
	this->textureGUI = new Texture("assets/textures/Gui_BG.png");

	/*****************************************TOWER BUTTONS ********************************************/
	string towerTextureList[4] = { "assets/textures/WallButton.png", "assets/textures/TurretButton.png", "assets/textures/FreezeButton.png", "assets/textures/MortarButton.png" };

	float curMinY;

	//Create tower buttons
	for (int x = 0; x < 4; x++) {

		float	xMin = (-1 + GUIBORDER_X) + ((1 - (x%2)) * GUIBORDER_X),
				yMin = (0.65 - ((2 * x + 1)*GUIBORDER_Y)) - ((x+1)*MULTIBUTTONSIZE_Y),
				xMax = (-1 + GUIBORDER_X) + (MULTIBUTTONSIZE_X) + ((1- (x % 2)) * GUIBORDER_X),
				yMax = (0.65 - ((2*x + 1)*GUIBORDER_Y)) - (x*MULTIBUTTONSIZE_Y);

		curMinY = yMin;

		glm::vec3 buttonPos((xMin + xMax) / 2, (yMin + yMax) / 2, 0);
		glm::vec2 buttonDim(xMax - xMin, yMax - yMin);
		Texture* text = new Texture(towerTextureList[x]);
		Button* butt = new Button(buttonPos, buttonDim, text, this->GUIShader);

		this->TowerButtons.push_back(pair<Button*, int>(butt, x));
	}

	string buttonTex = "assets/textures/button.png";
	float	xMin = (-1 + 1.5*GUIBORDER_X),
			yMin = -GUIBORDER_Y - MULTIBUTTONSIZE_Y,
			xMax = -1 + 1.5*GUIBORDER_X + MULTIBUTTONSIZE_X,
			yMax = -GUIBORDER_Y;

	glm::vec3 buttonPos((xMin + xMax) / 2, (yMin + yMax) / 2, 0);
	glm::vec2 buttonDim(xMax - xMin, yMax - yMin);
	Texture* text = new Texture(buttonTex);
	Button* butt = new Button(buttonPos, buttonDim, text, this->GUIShader);
	upgradeButton = butt;

	xMin = (-1 + 1.5*GUIBORDER_X);
	yMin = 2*GUIBORDER_Y;
	xMax = -1 + 1.5*GUIBORDER_X + MULTIBUTTONSIZE_X;
	yMax = 2*GUIBORDER_Y + MULTIBUTTONSIZE_Y;

	buttonPos = glm::vec3((xMin + xMax) / 2, (yMin + yMax) / 2, 0);
	buttonDim = glm::vec2(xMax - xMin, yMax - yMin);

	butt = new Button(buttonPos, buttonDim, text, this->GUIShader);
	sellButton = butt;

}

void GUI::Draw(glm::vec4 normalisedCoords){

	// Enable blending and alpha
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	this->GUIShader->Bind();
	this->GUIShader->setTextureLocation(0);
	this->GUIShader->setIsSelected(0);

	//Draw GUI background
	textureGUI->Bind(0);
	glEnable(GL_TEXTURE_2D);
	this->background->Draw();

	/*****************************************DRAW BUTTONS ********************************************/
	if (!activeTowerSelected) {
		for (pair<Button*, int> butt : TowerButtons) {
			Box test = butt.first->button->getAABB();
			//if button is selected, keep selected
			if (butt.first->isSelected == 2) {
				butt.first->Draw();
			}
			//if not selected check to see if hovering over if so then set
			else if (butt.first->checkCollision(normalisedCoords))
			{
				butt.first->isSelected = 1;
				butt.first->Draw();
			}
			//if not hovering over and not selected then set to 0
			else
			{
				butt.first->isSelected = 0;
				butt.first->Draw();
			}
		}
	}
	else {
		if (targetTower->getType() != WALL) {
			if (upgradeButton->isSelected == 2) {
				upgradeButton->Draw();
				upgradeButton->isSelected = 0;
			}
			//if not selected check to see if hovering over if so then set
			else if (upgradeButton->checkCollision(normalisedCoords))
			{
				upgradeButton->isSelected = 1;
				upgradeButton->Draw();
			}
			//if not hovering over and not selected then set to 0
			else
			{
				upgradeButton->isSelected = 0;
				upgradeButton->Draw();
			}
		}

		if (sellButton->isSelected == 2) {
			sellButton->Draw();
		}
		//if not selected check to see if hovering over if so then set
		else if (sellButton->checkCollision(normalisedCoords))
		{
			sellButton->isSelected = 1;
			sellButton->Draw();
		}
		//if not hovering over and not selected then set to 0
		else
		{
			sellButton->isSelected = 0;
			sellButton->Draw();
		}

	}
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
}

//Method that checks the buttons for collision with the mouse
void GUI::checkButtonCollision(glm::vec4 normalisedCoords){

	if (!activeTowerSelected) {
		selectedTower = -1;
		/*****************************************CHECK GRID SIZE BUTTONS ********************************************/
		for (pair<Button*, int> butt : TowerButtons) {
			butt.first->isSelected = 0;
			// Check for the mouse intersection with the buttons of the GUI
			if (butt.first->checkCollision(normalisedCoords)) {
				butt.first->isSelected = 2;
				selectedTower = butt.second;
			}
		}
	}
	else {
		if (sellButton->checkCollision(normalisedCoords)) {
			sellButton->isSelected = 2;
		}
		if (targetTower->getType() != WALL) {
			if (upgradeButton->checkCollision(normalisedCoords)) {
				upgradeButton->isSelected = 2;
			}
		}
	}
}

//Method that checks the GUI background for collision with the mouse
bool GUI::checkGUIcollision(glm::vec4 normalisedCoords){
	Box test = background->getAABB();
	// Check for the mouse intersection with the buttons of the GUI
	return (normalisedCoords.x < test.maxX && normalisedCoords.x > test.minX && normalisedCoords.y < test.maxY && normalisedCoords.y > test.minY);
}

void GUI::resetGUI()
{
	selectedTower = -1;
	for (pair<Button*, int> butt : TowerButtons) {
		butt.first->isSelected = 0;
	}
	targetTower = nullptr;
	activeTowerSelected = false;
	upgradeButton->isSelected = 0;
	sellButton->isSelected = 0;
}

